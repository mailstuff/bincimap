dnl This program is free software; you can redistribute it and/or modify
dnl it under the terms of the GNU General Public License as published by
dnl the Free Software Foundation; either version 2 of the License, or
dnl (at your option) any later version.

dnl You should have received a copy of the GNU General Public License
dnl along with this program; if not, write to the Free Software
dnl Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307,
dnl USA.

dnl M4 macros for Binc IMAP

AC_DEFUN([BINC_ACCEPT_PREFIX_ARGS],
    [
    dnl Check prefix
    prefix=$(eval echo $(eval echo ${prefix}))
    AC_MSG_CHECKING(--prefix)
    if [[ "x$prefix" = "xNONE" ]]; then
       AC_MSG_RESULT([/usr/local])
       prefix=/usr/local
    else
       AC_MSG_RESULT(using $prefix)
    fi
    AC_SUBST(prefix)
    
    dnl Check bindir
    bindir=$(eval echo $(eval echo ${bindir}))
    AC_MSG_CHECKING(--bindir)
    if [[ "x$bindir" = "xNONE/bin" ]]; then
       AC_MSG_RESULT([$prefix/bin])
       bindir=$prefix/bin
    else
       AC_MSG_RESULT(using $bindir)
    fi
    AC_SUBST(bindir)
    
    dnl Check sysconfdir   
    sysconfdir=$(eval echo $(eval echo ${sysconfdir}))
    AC_MSG_CHECKING(--sysconfdir)
    if [[ "x$sysconfdir" = "xNONE/etc" ]]; then
       AC_MSG_RESULT([$prefix/etc])
       sysconfdir=$prefix/etc
    else
       AC_MSG_RESULT(using $sysconfdir)
    fi
    AC_SUBST(sysconfdir)
    
    dnl Check localstatedir
    localstatedir=$(eval echo $(eval echo ${localstatedir}))
    AC_MSG_CHECKING(--localstatedir)
    if [[ "x$localstatedir" = "xNONE/var" ]]; then
       AC_MSG_RESULT([$prefix/var])
       localstatedir=$prefix/var
    else
       AC_MSG_RESULT(using $localstatedir)
    fi
    AC_SUBST(sysconfdir)
    
    dnl Check datadir
    datadir=$(eval echo $(eval echo ${datadir}))
    AC_MSG_CHECKING(--datadir)
    if [[ "x$datadir" = "xNONE/share" ]]; then
       AC_MSG_RESULT([$prefix/share])
       datadir=$prefix/share
    else
       AC_MSG_RESULT(using $datadir)
    fi
    AC_SUBST(datadir)

    dnl Check mandir
    mandir=$(eval echo $(eval echo ${mandir}))
    AC_MSG_CHECKING(--mandir)
    if [[ "x$datadir" = "xNONE/man" ]]; then
       AC_MSG_RESULT([$prefix/man])
       datadir=$prefix/man
    else
       AC_MSG_RESULT(using $man)
    fi
    AC_SUBST(mandir)

    ]
)


dnl Add /usr/kerberos/include to include path if this path
dnl exists.
AC_DEFUN([BINC_CHECK_KERBEROS_INCLUDE],
    [
    AC_MSG_CHECKING(wether to include /usr/kerberos/include)
    if [[ -e /usr/kerberos/include ]]; then
      AC_MSG_RESULT([yes])
      CXXFLAGS="-I/usr/kerberos/include $CXXFLAGS"
    else
      AC_MSG_RESULT([not necessary])
    fi
    ]
)

dnl Check if we should compile static or shared binaries.
AC_DEFUN([BINC_CHECK_STATIC],
    [
    AC_ARG_ENABLE(static,
         AC_HELP_STRING([--enable-static], [Enable static compile]),
        [ if [[ "x$enableval" != "xno" ]]; then STATIC="-static"; fi ], [])
    AC_SUBST(STATIC)

    AC_MSG_CHECKING(wether to compile static or shared)
    if [[ "x$STATIC" = "x" ]]; then
        AC_MSG_RESULT(shared)
    else
        AC_MSG_RESULT(static)
    fi
    ]
)

dnl Check if we should compile with optimization or not.
AC_DEFUN([BINC_CHECK_OPTIMIZATION],
    [
    AC_ARG_WITH(optimization,
        AC_HELP_STRING([--with-optimization], [Compile with -O2 (default)])
AC_HELP_STRING([--without-optimization], [Compile with -O0]),
        [ if [[ "x$withval" != "xno" ]]; then WITH_OPTIMIZATION=1; fi ],
        WITH_OPTIMIZATION=1)

    AC_MSG_CHECKING(wether to compile with optimization)
    if [[ "x$WITH_OPTIMIZATION" != "x1" ]]; then
        AC_MSG_RESULT(no)
        CXXFLAGS="$CXXFLAGS -O0"
        CXXFLAGS=`echo $CXXFLAGS | sed 's/ -O2//'`
    else
        AC_MSG_RESULT(yes)
    fi
    ]
)

dnl Check if we should link against libdl
AC_DEFUN([BINC_CHECK_LIBDL],
    [
    AC_CHECK_LIB(dl, dlopen, LIBDL=-ldl, LIBDL=)
    AC_SUBST(LIBDL)
    ]
)

dnl Detect the chroot jail settings
AC_DEFUN([BINC_CHECK_JAILSETTINGS],
    [
    JAILPATH=$prefix
    JAILUSER=nobody
    JAILGROUP=nobody

    dnl Check the jail path argument
    AC_ARG_WITH(jail-path,
        AC_HELP_STRING([--with-jail-path],
           [Set path to chroot jail]),
           [
               if [[ "x$withval" != "xno" -a "x$withval" != "xyes" ]]; then
                   JAILPATH=$withval;
               fi
           ],)

    dnl Check the jail path argument
    AC_ARG_WITH(jail-user,
        AC_HELP_STRING([--with-jail-user],
           [Set chroot jail user]),
           [
               if [[ "x$withval" != "xno" -a "x$withval" != "xyes" ]]; then
                   JAILUSER=$withval;
               fi
           ],)

    dnl Check the jail path argument
    AC_ARG_WITH(jail-group,
        AC_HELP_STRING([--with-jail-group],
           [Set chroot jail group]),
           [
               if [[ "x$withval" != "xno" -a "x$withval" != "xyes" ]]; then
                   JAILGROUP=$withval;
               fi
           ],)

    AC_DEFINE_UNQUOTED(CHROOT_JAIL_PATH, "$JAILPATH", Path to chroot jail)
    AC_DEFINE_UNQUOTED(CHROOT_JAIL_USER, "$JAILUSER", Unprivileged chroot user)
    AC_DEFINE_UNQUOTED(CHROOT_JAIL_GROUP, "$JAILGROUP", Unprivileged chroot group)
    ]
)

dnl Detect OpenSSL include file path and headers.
AC_DEFUN([BINC_CHECK_OPENSSL],
    [
    dnl Accept the --with-openssl-lib path configure option.
    AC_ARG_WITH(openssl-lib,
        AC_HELP_STRING([--with-openssl-lib],
           [Set path to OpenSSL libraries]),
           [
               if [[ "x$withval" != "xno" -a "x$withval" != "xyes" ]]; then
                   SSLLIBPATH=$withval;
               fi
           ],)

    dnl Accept the --with-openssl-include path configure option.
    AC_ARG_WITH(openssl-include,
        AC_HELP_STRING([--with-openssl-include],
            [Set path to OpenSSL headers]),
            [
                if [[ "x$withval" != "xno" -a "x$withval" != "xyes" ]]; then
                    SSLINCLUDEPATH=$withval;
                fi
            ],)

    dnl Accept the --with-ssl and --without-ssl configure option.
    AC_ARG_WITH(ssl,
        AC_HELP_STRING([--with-ssl], [Enable SSL support (default)])
AC_HELP_STRING([--without-ssl], [Disable SSL support]),
        [ if [[ "x$withval" != "xno" ]]; then WITH_SSL=1; fi ],
        WITH_SSL=1)

    dnl Unless asked not to, check for the SSL libraries.
    if [[ "$WITH_SSL" = "1" ]]; then

        dnl Check if we need to link with -lsocket.
        AC_MSG_CHECKING(whether -lsocket is available)
        export LDTMP=$LIBS
        export LIBS="$LIBS -lsocket"
        AC_TRY_LINK([], [], LIBSOCKET="-lsocket";
            AC_MSG_RESULT(yes), AC_MSG_RESULT(no))
        export LIBS=$LDTMP

        dnl A list of paths to use to search for OpenSSL headers.
        dnl This list should be extended as we go to reflect the 95%
        dnl most common locations of the OpenSSL headers.
        SSL_INCLUDE_PROBEPATHS="/usr/include \
                                /usr/local/include \
                                /usr/local/ssl/include \
                                /usr/local/openssl/include \
                                /opt/ssl/include \
                                /opt/openssl/include"

        dnl Search for the OpenSSL include files by attempting to
        dnl compile a test program using different -I <path> options.
        AC_MSG_CHECKING(for OpenSSL includes)
        export CXXTMP="$CXXFLAGS"
        for k in . $SSLINCLUDEPATH $SSL_INCLUDE_PROBEPATHS; do
            export CXXFLAGS="$CXXTMP -I$k"
            AC_TRY_COMPILE([#include <openssl/ssl.h>],
                           [SSL_write(0, 0, 0);],
                           INCLUDESSL="-I$k";
                           if [[[ "$k" != "." ]]]; then
                               AC_MSG_RESULT($k);
                           else
                               AC_MSG_RESULT(yes);
                           fi, [])

            if [[ "x$INCLUDESSL" != "x" ]]; then
                break;
            fi
        done

        if [[ "x$INCLUDESSL" = "x" ]]; then
            AC_MSG_RESULT(not found. Try --with-openssl-include.)
            export CXXFLAGS="$CXXTMP"
        fi
        if [[ "x$INCLUDESSL" = "x-I." ]]; then
            INCLUDESSL=""
        fi

        dnl A list of paths to use to search for OpenSSL libraries.
        dnl This list should be extended as we go to reflect the 95%
        dnl most common locations of the OpenSSL libraries.
        SSL_LIB_PROBEPATHS="/usr/lib \
                            /usr/local/lib \
                            /usr/local/ssl/lib \
                            /usr/local/openssl/lib \
                            /opt/ssl/lib \
                            /opt/openssl/lib"

        dnl Search for the OpenSSL library files by attempting to
        dnl link a test program using different -L <path> options.
        AC_MSG_CHECKING(for OpenSSL libraries)
        export LIBTMP=$LIBS
        export LDTMP=$LDFLAGS
        for k in . $SSLLIBPATH $SSL_LIB_PROBEPATHS; do
            export LDFLAGS="$LDTMP $STATIC -L$k"
            export LIBS="$LIBTMP -lssl -lcrypto $LIBSOCKET $LIBDL"
            AC_TRY_LINK([#include <openssl/ssl.h>],
                        [SSL_write(0, 0, 0);],
                        LIBSSL="-L$k -lssl -lcrypto $LIBSOCKET";
                        if [[[ "$k" != "." ]]]; then
                            AC_MSG_RESULT($k);
                        else
                            AC_MSG_RESULT(yes);
                        fi, [])

            if [[ "x$LIBSSL" != "x" ]]; then
                break;
            fi
        done
        export LIBS=$LIBTMP
        export LDFLAGS=$LDTMP

        if [[ "x$LIBSSL" = "x" ]]; then
            AC_MSG_RESULT(not found. Try --with-openssl-libs.)
        else
            AC_DEFINE(WITH_SSL, 1, [Define to 1 if SSL support is included.])
            LIBSSL=`echo $LIBSSL | sed 's/\-L\\. //'`
        fi
    else
        AC_MSG_RESULT(no)
    fi
    AC_SUBST(LIBSSL)
    ]
)

dnl Check for largefile support ( > 2GB files)
AC_DEFUN([BINC_CHECK_LARGEFILE],
    [
    AC_MSG_CHECKING(for O_LARGEFILE support)
    AC_TRY_COMPILE([
                    #include <sys/types.h>
                    #include <sys/stat.h>
                    #include <fcntl.h>
                    int i = O_LARGEFILE;
                   ],
                   [],
                   AC_MSG_RESULT([yes])
                   AC_DEFINE(HAVE_OLARGEFILE,, [support for O_LARGEFILE])
                   HAVE_OLARGEFILE=1,
                   AC_MSG_RESULT([no]))
    ]
)

dnl Check for file system change notification support.
AC_DEFUN([BINC_CHECK_F_NOTIFY],
    [
    AC_MSG_CHECKING(for F_NOTIFY support)
    AC_TRY_COMPILE([
                    #define _GNU_SOURCE
                    #include <fcntl.h>
                    int i = F_NOTIFY;
                   ],
                   [],
                   AC_MSG_RESULT([yes]);
                   AC_DEFINE(HAVE_FNOTIFY,, [support for F_NOTIFY])
                   HAVE_FNOTIFY=1,
                   AC_MSG_RESULT([no]))
    ]
)

dnl Show a summary of the configure step
AC_DEFUN([BINC_SUMMARY],
    [
        if [[ "x$LIBSSL" = "x" ]]; then
            SSLENABLED=disabled
        else
            SSLENABLED=enabled
        fi

        if [[ "x$HAVE_OLARGEFILE" = "x" ]]; then
            LARGEFILEENABLED=disabled
        else
            LARGEFILEENABLED=enabled
        fi

        if [[ "x$HAVE_FNOTIFY" = "x" ]]; then
            FNOTIFYENABLED=disabled
        else
            FNOTIFYENABLED=enabled
        fi

        if [[ "x$STATIC" = "x" ]]; then
            STATICORSHARED=shared
        else
            STATICORSHARED=static
        fi

        echo
        echo "    SSL (Secure Sockets Layer)  - $SSLENABLED"
        echo "    Large file support          - $LARGEFILEENABLED"
        echo "    File directory notification - $FNOTIFYENABLED"
        echo
        echo "    Static or shared binaries   - $STATICORSHARED"
        echo
        echo Ready to make.
    ]
)
