# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307,
# USA.

#-----------------------------------------------------------------------
# package parameters

# version and release can be overridden on the rpmbuild command-line
# with --define 'release 2', etc.

%{!?version:%define version @VERSION@}
%{!?release:%define release 1}

# --without ssl   -- build without SSL support (default is --with ssl)
# --with inusr    -- install into /usr instead of /opt

#%{!?_without_ssl:%define _without_ssl --without-ssl}
%{!?_with_inusr:%define inopt 1}

#-----------------------------------------------------------------------
Summary: Binc IMAP server
Name: @PACKAGE@
Version: %{version}
Release: %{release}
URL: http://www.bincimap.org/
Source0: %{name}-%{version}.tar.gz
License: GPL
Group: System Environment/Daemons
BuildRoot: %{_tmppath}/%{name}-%{version}-root
Packager: Andreas Aardal Hanssen <andreas-binc@bincimap.org>
Vendor: Andreas Aardal Hanssen <andreas-binc@bincimap.org>
%{?_with_ssl:BuildRequires: openssl-devel}
%{?_with_ssl:Requires: openssl}
%{!?inopt:Prefix: /usr}

# Path settings for /opt installation (for /usr installation
# the default RedHat macros work as is)
%{?inopt:%define _prefix	/opt/@PACKAGE@}
%{?inopt:Prefix: %{_prefix}}
%{?inopt:%define _sysconfdir	/etc/opt}
%{?inopt:%define _localstatedir	/var/opt}
%{?inopt:%define _docdir	%{_prefix}/doc}
%{?inopt:%define _mandir	%{_prefix}/man}
%{?inopt:%define _sbindir	%{_prefix}/bin}

#-----------------------------------------------------------------------
%description

Binc IMAP is an IMAP server, written in C++ for the Linux platform. It
supports Dan J. Bernstein's Maildir format and checkpassword
authentication.

As an alternative to existing similar IMAP servers, Binc IMAP strives
to be

* very easy to install and use, but robust, stable and secure
* absolutely compliant with the IMAP4rev1 protocol
* simple and modular in design, making it very easy for
  third parties to utilize the source code and enhance the
  product.

Binc IMAP is released under the GNU General Public License.

#-----------------------------------------------------------------------
%prep
%setup -q

#-----------------------------------------------------------------------
%build

STATIC=

CXXFLAGS="-std=c++11 -O2 -s -I/usr/kerberos/include"
%configure $STATIC --prefix=/opt/@PACKAGE@ --sysconfdir=/etc/opt/@PACKAGE@ --localstatedir=/var/opt

make

#-----------------------------------------------------------------------
%install
rm -rf $RPM_BUILD_ROOT

# shouldn't we be using a 'make install' target for all this?

PRE=$RPM_BUILD_ROOT%{_prefix}
BIN=$RPM_BUILD_ROOT%{_sbindir}
ETC=$RPM_BUILD_ROOT%{_sysconfdir}/@PACKAGE@
SCR=$RPM_BUILD_ROOT%{_sysconfdir}/@PACKAGE@/scripts
VAR=$RPM_BUILD_ROOT%{_localstatedir}
MAN=$RPM_BUILD_ROOT%{_mandir}

# Directory structure
install -d $BIN
install -d $ETC/xinetd
install -d $SCR
install -d $ETC/service/imap/log
install -d $ETC/service/imaps/log
install -d $MAN/man1
install -d $MAN/man5
install -d $VAR/log/@PACKAGE@
install -d $VAR/log/@PACKAGE@-ssl

# Binaries
install src/bincimapd		$BIN
install src/bincimap-up		$BIN

# Config files
install conf/bincimap.conf	$ETC
install conf/xinetd-bincimap	$ETC/xinetd/imap
install conf/xinetd-bincimaps	$ETC/xinetd/imaps

install conf/checkpassword.pl	$SCR
install conf/toimapdir		$SCR
#install conf/tomaildir++	$SCR
install conf/xinetd-bincimaps	$ETC/xinetd/imaps
install conf/xinetd-bincimaps	$ETC/xinetd/imaps

install service/run		$ETC/service/imap/run
install service/log/run		$ETC/service/imap/log/run
install service/run-ssl		$ETC/service/imaps/run
install service/log/run-ssl	$ETC/service/imaps/log/run

# Documentation
# (the non-man doc files are taken care of with the %doc macro)
install man/bincimapd.1		$MAN/man1
install man/bincimap-up.1	$MAN/man1
install man/bincimap.conf.5	$MAN/man5

#-----------------------------------------------------------------------
%clean
#rm -rf $RPM_BUILD_ROOT

#-----------------------------------------------------------------------
%files
%defattr(755,root,root)

# Documentation files (other than man pages)
%doc README README.SSL COPYING COPYING.OpenSSL ChangeLog 
%doc doc/bincimap.css doc/bincimap-*.html
# %doc doc/manual/bincimap-manual.{dvi,ps}
%doc doc/rfc2060.txt doc/rfc2683.txt

# These dirs belong to the package only if in /opt
%{?inopt:%dir %{_prefix}}
%{?inopt:%dir %{_bindir}}
%{?inopt:%dir %{_mandir}}
%{?inopt:%dir %{_mandir}/*}

# Directories
%dir %{_sysconfdir}/@PACKAGE@
%dir %{_sysconfdir}/@PACKAGE@/service
%dir %{_sysconfdir}/@PACKAGE@/service/imap
%dir %{_sysconfdir}/@PACKAGE@/service/imap/log
%dir %{_sysconfdir}/@PACKAGE@/service/imaps
%dir %{_sysconfdir}/@PACKAGE@/service/imaps/log
%dir %{_sysconfdir}/@PACKAGE@/xinetd
%dir %{_localstatedir}/log/@PACKAGE@
%dir %{_localstatedir}/log/@PACKAGE@-ssl

# Binaries
%defattr(755,root,root)
%{_sbindir}/bincimapd
%{_sbindir}/bincimap-up
%{_sbindir}/bincimap-updatecache
#%{_sysconfdir}/@PACKAGE@/scripts/tomaildir++
%{_sysconfdir}/@PACKAGE@/scripts/toimapdir
%{_sysconfdir}/@PACKAGE@/scripts/checkpassword.pl

# Config files
%config %{_sysconfdir}/@PACKAGE@/service/imap/run
%config %{_sysconfdir}/@PACKAGE@/service/imap/log/run
%config %{_sysconfdir}/@PACKAGE@/service/imaps/run
%config %{_sysconfdir}/@PACKAGE@/service/imaps/log/run
%defattr(644,root,root)
%config %{_sysconfdir}/@PACKAGE@/bincimap.conf
%config %{_sysconfdir}/@PACKAGE@/xinetd/imap
%config %{_sysconfdir}/@PACKAGE@/xinetd/imaps

# Man pages
%{_mandir}/man1/bincimap.1*

#-----------------------------------------------------------------------
%changelog

* Mon Jul 12 2004 Andreas Aardal Hanssen <andreas-binc@hanssen.name>
- Integrated changes from 1.2.

* Wed Jul 07 2004 Andreas Aardal Hanssen <andreas-binc@hanssen.name>
- Added support for the Maildir P (passed) flag.

* Tue Jul 06 2004 Andreas Aardal Hanssen <andreas-binc@hanssen.name>
- Added CHILDREN extension.

* Sun Jul 04 2004 Andreas Aardal Hanssen <andreas-binc@hanssen.name>
- Fixed a bug with concurrent access when both clients issued
  EXPUNGE requests on messages that had arrived within the same
  session.

* Mon Jun 28 2004 Andreas Aardal Hanssen <andreas-binc@hanssen.name>
- 70% !! Speed increase in MIME parser. Thanks to calltree/valgrind
  and kcachegrind.

* Sat Jun 26 2004 Andreas Aardal Hanssen <andreas-binc@hanssen.name>
- Introduced MimeInputSource, getting one step closer to being able
  to provide a custom input source for the MIME parser. It's still
  no where close to perfect, as the input source is a global
  object.

* Thu Jun 17 2004 Andreas Aardal Hanssen <andreas-binc@hanssen.name>
- Added NAMESPACE support.
- Added custom flags support + keywords support. 

* Tue Jun 08 2004 Andreas Aardal Hanssen <andreas-binc@hanssen.name>
- Made some essential Depot functions virtual.
- Moved the subscriptions list from Session to Depot, as virtual
  functions, to allow reimplementors to store the subscription
  list in mysterious ways.
- Added the next generation IO classes, without connecting them to
  the server yet.

* Mon Jun 07 2004 Andreas Aardal Hanssen <andreas-binc@hanssen.name>
- Added a fix to operator-idle.cc, which didn't work at all.
- Advertise IDLE in all states. 	
- Added new tool: bincimap-updatecache. This is used to update the
  cache file on delivery, speeding up selects.

* Thu Jun 03 2004 Andreas Aardal Hanssen <andreas-binc@hanssen.name>
- Don't trim entries read from .bincimap-subscribed.
  Allow mailboxes that start with a space.
- Fix a bug with subscribing to mailboxes. Now, mailboxes can only
  be subscribed to once.
- Allow FETCH to give FLAGS, EXISTS and RECENT
  notifications for tighter synchronization with the
  depot.
- Remove instances of "-I. " instead of " -I.", as the
  latter would remove " -I./foo", which was not the intention.

* Mon May 31 2004 Andreas Aardal Hanssen <andreas-binc@hanssen.name>
- Explicitly define _USE_GNU for O_DIRECTORY and
  F_NOTIFY support.
- _USE_GNU -> __USE_GNU
- Compile fix for FreeBSD (pid_t declaration).

* Sat May 29 2004 Andreas Aardal Hanssen <andreas-binc@hanssen.name>
- Allow older autoconf versions by removing double quoting.
- Clean up the compile line, don't -I. all the time.
- Have the OpenSSL tests just say "yes" if the libraries
  or headers were detected without including extra path
  info.
- Add script for making it easy to do daily cron job
  based testing of snapshots.
- Move all scripts into a scripts/ directory.
- Add Henry Baragar's migration scripts and fix
  the install.

* Tue May 25 2004 Andreas Aardal Hanssen <andreas-binc@hanssen.name>
- Added Henry Baragar's patches to fix the untagged NO responses
  for unsupported commands. The responses are now tagged.
- Renamed the SHOW_VERSION_IN_GREETING config option to
  VERBOSE_GREETING, and by default only a very short
  greeting is provided. Enabling this option will give the version
  string and the copyright notice.

* Sun May 23 2004 Andreas Aardal Hanssen <andreas-binc@hanssen.name>
- Cleanups in IDLE code.
- Allow the argument parser to handle unqualified arguments.
- Added globals.h for global constants which should never be
  manipulated.
- Renamed bincimap.conf to bincimap-config (since it's a script now)
- Introduced bincimap-localconfig, a skeleton script which is run
  right before bincimapd.
- Added handling of the --enable-ssl command line argument.
- Wrote a nice man page to rule them all: bincimap.1.
- UID STORE now responds with a UID data member as part of the
  untagged FETCH responses.
- Added Daniel James' patch which fixes the fetch att RFC822.size
  to prevent the header from being included.

* Thu May 20 2004 Andreas Aardal Hanssen <andreas-binc@hanssen.name>
- Support for IDLE though polling or F_NOTIFY.
- Fixed a bug in NOOP where a failed pending update would have the
  server say BYE without actually failing the command.
- Configuration settings are now read from the environment, not from
  a conf file. The config file has been converted to a shell script
  which exports certain variables before executing its first argument.
- Several configurable options have been removed. Binc IMAP now
  has only 9 main options, and 7 SSL options.
- The local configuration map has been removed, and the global map
  has been flattened. No more configuration sections.
- Fixed problem with the sequence set '*'. Binc IMAP claimed no
  messages matched this set, but now the highest UID or sequence
  number matches.

#-----------------------------------------------------------------------
# Local variables:
# mode: rpm-spec
# End:
