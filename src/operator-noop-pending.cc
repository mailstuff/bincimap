/* -*- mode:c++;c-basic-offset:2 -*- */
/*  --------------------------------------------------------------------
 *  Filename:
 *    operator-noop-pending.cc
 *  
 *  Description:
 *    Operator for the NOOP command, with pending extension
 *  --------------------------------------------------------------------
 *  Copyright 2002-2005 Andreas Aardal Hanssen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *  --------------------------------------------------------------------
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string>
#include <iostream>

#include "mailbox.h"
#include "pendingupdates.h"

#include "recursivedescent.h"
#include "session.h"
#include "depot.h"
#include "operators.h"

using namespace ::std;
using namespace Binc;

//----------------------------------------------------------------------
NoopPendingOperator::NoopPendingOperator(void) : NoopOperator()
{
}

//----------------------------------------------------------------------
NoopPendingOperator::~NoopPendingOperator(void)
{
}

//----------------------------------------------------------------------
Operator::ProcessResult NoopPendingOperator::process(Depot &depot,
						     Request &command)
{
  Mailbox *mailbox = depot.getSelected();
  if (mailbox) {
    pendingUpdates(mailbox, 
		   PendingUpdates::EXPUNGE
		   | PendingUpdates::EXISTS 
		   | PendingUpdates::RECENT 
		   | PendingUpdates::FLAGS, true);
  }

  return OK;
}
