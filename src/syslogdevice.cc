/*-*-mode:c++-*-*/
/*  --------------------------------------------------------------------
 *  Filename:
 *    src/syslogdevice.cc
 *  
 *  Description:
 *    Implementation of the SyslogDevice class.
 *  --------------------------------------------------------------------
 *  Copyright 2002, 2003 Andreas Aardal Hanssen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *  --------------------------------------------------------------------
 */
#include "syslogdevice.h"
#include <string>

#include <syslog.h>

using namespace ::std;
using namespace ::Binc;

//------------------------------------------------------------------------
string SyslogDevice::ident;

//------------------------------------------------------------------------
SyslogDevice::SyslogDevice(int f, const char *i, int o, int fa) 
  : IODevice(f), option(o), facility(fa), priority(LOG_INFO)
{
  ident = i;
  openlog(ident.c_str(), option, facility);
}

//------------------------------------------------------------------------
SyslogDevice::~SyslogDevice(void)
{
  closelog();
}

//------------------------------------------------------------------------
string SyslogDevice::service(void) const
{
  return "log";
}

//------------------------------------------------------------------------
bool SyslogDevice::waitForWrite(void) const
{
  return true;
}

//------------------------------------------------------------------------
bool SyslogDevice::waitForRead(void) const
{
  return false;
}

//------------------------------------------------------------------------
IODevice::WriteResult SyslogDevice::write(void)
{
  string out;
  string::const_iterator i = outputBuffer.str().begin();
  string::const_iterator ie = outputBuffer.str().end();
  for (; i != ie; ++i) {
    if (*i == '\n') {
      syslog(priority, out.c_str(), out.size());
      out = "";
    } else if (*i != '\r')
      out += *i;
  }
  
  if (out != "")
    syslog(priority, out.c_str(), out.size());

  outputBuffer.clear();
  return WriteDone;
}

//------------------------------------------------------------------------
bool SyslogDevice::fillInputBuffer(void)
{
  return false;
}
