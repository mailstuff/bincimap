/* -*- mode:c++;c-basic-offset:2 -*- */
/*  --------------------------------------------------------------------
 *  Filename:
 *    maildir-writecache.cc
 *  
 *  Description:
 *    Implementation of the Maildir class.
 *  --------------------------------------------------------------------
 *  Copyright 2002-2005 Andreas Aardal Hanssen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *  --------------------------------------------------------------------
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <dirent.h>
#include <fcntl.h>
#include <unistd.h>

#include "maildir.h"

using namespace ::std;

//------------------------------------------------------------------------
bool Binc::Maildir::writeCache(void)
{
  if (readOnly)
    return true;

  char *safename = strdup((path + "/.bincimap-cache-tmp-XXXXXX").c_str());
  int fd = mkstemp(safename);
  if (!fd) {
    free(safename);
    return false;
  }

  string safeName = safename;
  free(safename);

  FILE *fp = fdopen(fd, "w");
  if (!fp) {
    unlink(safeName.c_str());
    return false;
  }

  if (uidvalidity == 0 || uidnext == 0) {
    uidvalidity = time(0);
    uidnext = messages.size() + 1;
  }

  fprintf(fp, "%s %u %u\n", "BINC-CACHE-1.0", uidvalidity, uidnext);
  Mailbox::iterator i = begin(SequenceSet::all(), INCLUDE_EXPUNGED);
  for (; i != end(); ++i) {
    MaildirMessage &message = (MaildirMessage &)*i;
    fprintf(fp, "%u %u %u %s", message.getUID(), 
	   (unsigned int) message.getInternalDate(), message.getSize(),
	   message.getUnique().c_str());
    vector<string> cflags = message.getCustomFlags();
    for (vector<string>::const_iterator it = cflags.begin();
	 it != cflags.end(); ++it) {
      fprintf(fp, " %s", (*it).c_str());
    }
    fprintf(fp, "\n");
  }

  if (fflush(fp) || (fsync(fd) && (errno != EROFS || errno != EINVAL)) || fclose(fp)) {
    unlink(safeName.c_str());
    return false;
  }

  if (rename(safeName.c_str(), (path + "/bincimap-cache").c_str()) != 0) {
    unlink(safeName.c_str());
    return false;
  }

  int dfd = open(path.c_str(), O_RDONLY);
  if (dfd == -1 || (fsync(fd) && (errno != EROFS || errno != EINVAL)) || close(dfd)) {
    return false;
  }

  return true;
}
