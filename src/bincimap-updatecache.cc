/* -*- mode:c++;c-basic-offset:2 -*- */
/*  --------------------------------------------------------------------
 *  Filename:
 *    bincimap-updatecache.cc
 *  
 *  Description:
 *    Implementation of the bincimap-updatecache tool.
 *  --------------------------------------------------------------------
 *  Copyright 2002-2005 Andreas Aardal Hanssen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *  --------------------------------------------------------------------
 */
#include "depot.h"
#include "mailbox.h"
#include "maildir.h"
#include "session.h"

using namespace ::Binc;
using namespace ::std;

int main(int argc, char *argv[])
{
  if (argc < 2) {
    fprintf(stderr, "usage: %s <directory>", argv[0]);
    fprintf(stderr, "Updates the cache file in <directory>.\n");
    return 1;
  }

  Session &session = Session::getInstance();

  DepotFactory &depotfactory = DepotFactory::getInstance();
  depotfactory.assign(new IMAPdirDepot());
  depotfactory.assign(new MaildirPPDepot());

  string depottype = session.getEnv("DEPOT");
  if (depottype == "") depottype = "Maildir++";

  Depot *depot;
  if ((depot = depotfactory.get(depottype)) == 0) {
    fprintf(stderr, "Found no Depot for \"%s\". Please check " \
	    " your configurations file under the Mailbox section.\n",
	    depottype.c_str());
    return 1;
  }

  depot->assign(new Maildir());
  depot->setDefaultType("Maildir");

  Mailbox *mailbox = depot->get(depot->filenameToMailbox(argv[1]));

  if (!mailbox) {
    fprintf(stderr, "selecting mailbox failed: %s\n",
	    depot->getLastError().c_str());
    return 1;
  }

  if (!mailbox->selectMailbox(argv[1], argv[1])) {
    fprintf(stderr, "selecting mailbox failed: %s\n",
	    mailbox->getLastError().c_str());
    return 1;
  }

  mailbox->closeMailbox();

  return 0;
}
