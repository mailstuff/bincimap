/*-*-mode:c++-*-*/
/*  --------------------------------------------------------------------
 *  Filename:
 *    src/stdiodevice.cc
 *  
 *  Description:
 *    Implementation of the StdIODevice class.
 *  --------------------------------------------------------------------
 *  Copyright 2002, 2003 Andreas Aardal Hanssen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *  --------------------------------------------------------------------
 */
#include "stdiodevice.h"
#include <string>

#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <sys/time.h>
#include <unistd.h>
#include <errno.h>

using namespace ::std;
using namespace ::Binc;

//------------------------------------------------------------------------
StdIODevice::StdIODevice(int f) : IODevice(f)
{
}

//------------------------------------------------------------------------
StdIODevice::~StdIODevice(void)
{
}

//------------------------------------------------------------------------
string StdIODevice::service(void) const
{
  return "client";
}

//------------------------------------------------------------------------
bool StdIODevice::canRead(void) const
{
  size_t bytes;
  return ioctl(fileno(stdin), FIONREAD, (char *) &bytes) > 0;
}

//------------------------------------------------------------------------
bool StdIODevice::waitForWrite(void) const
{
  fd_set writeMask;
  FD_ZERO(&writeMask);
  FD_SET(fileno(stdout), &writeMask);

  struct timeval tv;
  tv.tv_sec = timeout;
  tv.tv_usec = 0;

  int result = select(fileno(stdout) + 1, 0, &writeMask,
		      0, timeout ? &tv : 0);
  if (result == 0)
      error = Timeout;
  return result > 0;
}

//------------------------------------------------------------------------
bool StdIODevice::waitForRead(void) const
{
  fd_set readMask;
  FD_ZERO(&readMask);
  FD_SET(fileno(stdin), &readMask);

  struct timeval tv;
  tv.tv_sec = timeout;
  tv.tv_usec = 0;

  int result = select(fileno(stdin) + 1, &readMask, 0,
		      0, timeout ? &tv : 0);
  if (result == 0)
      error = Timeout;
  return result > 0;
}

//------------------------------------------------------------------------
IODevice::WriteResult StdIODevice::write(void)
{
  for (;;) {
    ssize_t wrote = ::write(fileno(stdout), outputBuffer.str().c_str(),
			    outputBuffer.getSize());

    if (wrote == -1) {
      if (errno == EINTR)
	continue;
      else
	return WriteError;
    }

    outputBuffer.popString(wrote);

    if (wrote == (ssize_t) outputBuffer.getSize())
      return WriteDone;
    return WriteWait;
  }
}

//------------------------------------------------------------------------
bool StdIODevice::fillInputBuffer(void)
{
  if (!waitForRead())
    return false;

  char buf[4096];
  ssize_t red = read(fileno(stdin), buf, sizeof(buf) - 1);
  if (red <= 0)
    return false;

  buf[red] = '\0';
  inputBuffer << buf;
  return true;
}
