/* -*- mode:c++;c-basic-offset:2 -*- */
/*  --------------------------------------------------------------------
 *  Filename:
 *    imapserver.cc
 *  
 *  Description:
 *    Implementation of the IMAPServer class.
 *  --------------------------------------------------------------------
 *  Copyright 2002-2005 Andreas Aardal Hanssen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *  --------------------------------------------------------------------
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "broker.h"
#include "globals.h"
#include "imapparser.h"
#include "imapserver.h"
#include "iodevice.h"
#include "iofactory.h"
#include "session.h"

using namespace ::Binc;
using namespace ::std;

namespace Binc {
  void showGreeting(void);
};

IMAPServer::IMAPServer(int argc, char **argv)
{
  this->argc = argc;
  this->argv = argv;
  stubMode = false;
  Session::getInstance().setState(Session::AUTHENTICATED);
}

IMAPServer::~IMAPServer(void)
{
}

int IMAPServer::initialize(void)
{
  Session &session = Session::getInstance();
  if (!session.initialize(argc, argv))
    return 111;
  return 0;
}

void IMAPServer::prepareForNextRequest(void)
{
    serverStatus = OK;

    bincClient.setFlags(IODevice::HasInputLimit);
    bincClient.flush();
    bincClient.setMaxInputBufferSize(INPUT_BUFFER_LIMIT);

    Session::getInstance().setLastError("");
    Session::getInstance().clearResponseCode();
}

int IMAPServer::runStub(void)
{
  bincDebug << "IMAPServer::runStub(), running stub" << endl;
  stubMode = true;
  Session::getInstance().setState(Session::NONAUTHENTICATED);
  return run();
}

int IMAPServer::run(void)
{
  Session &session = Session::getInstance();
  if (session.hasEnv("LOG_LEVEL")) {
    bincInfo.setOutputLevelLimit(IODevice::LogLevel(atoi(session.getEnv("LOG_LEVEL"))));
  } else {
    bincInfo.setOutputLevelLimit(IODevice::InfoLevel);
  }

  bincDebug << "IMAPServer::run(), started server" << endl;

  if (stubMode) {
    if (session.hasEnv("PROTOCOLDUMP"))
      bincClient.enableProtocolDumping();
    bincInfo << "<" << session.getIP() << "> connected" << endl;
    showGreeting();
  } else {
    bincInfo << "<" << session.getEnv("USER") << "> logged in" << endl;
  }

  bincInfo.flush();

  do {
    bincDebug << "IMAPServer::run(), preparing for next request" << endl;

    prepareForNextRequest();

    // Find the current state's broker.
    BrokerFactory &brokerFactory = BrokerFactory::getInstance();
    Broker *broker = brokerFactory.getBroker(session.getState());

    bincDebug << "IMAPServer::run(), found broker " << reinterpret_cast<intptr_t>(broker)
	      << " for state " << session.getState() << endl;

    bool skipToNextRequest = false;

    // Parse the stub of the IMAP request.
    Request clientRequest;
    int stubParseResult = broker->parseStub(clientRequest);
    if (stubParseResult == Operator::TIMEOUT) {
      serverStatus = Timeout;
      break;
    } else if (stubParseResult == Operator::REJECT) {
      serverStatus = RequestRejected;
    } else if (stubParseResult == Operator::ERROR) {
      serverStatus = RequestError;
    } else {
      // Find an operator that recognizes the name of the request, and
      // have it continue the parsing.
      Operator *o = broker->get(clientRequest.getName());
      if (!o) {
	serverStatus = RequestRejected;
	string err = "The command \"";
	if (clientRequest.getUidMode()) err += "UID ";
	err += clientRequest.getName();
	err += "\" is unsupported in this state. ";
	session.setLastError(err);
	skipToNextRequest = true;
      } else {
	int parseResult = o->parse(clientRequest);
	if (parseResult == Operator::TIMEOUT) {
	  serverStatus = Timeout;
	} else if (parseResult == Operator::REJECT) {
	  serverStatus = RequestRejected;
	} else if (parseResult == Operator::ERROR) {
	  serverStatus = RequestError;
	} else {

	  session.addStatement();
	  Depot *dep = session.getDepot();

	  int processResult = o->process(*dep, clientRequest);
	  if (processResult == Operator::OK) {
	  } else if (processResult == Operator::NO) {
	    serverStatus = RequestRejected;
	  } else if (processResult == Operator::BAD) {
	    serverStatus = RequestError;
	  } else if (processResult == Operator::NOTHING) {
	  } else if (processResult == Operator::ABORT) {
	    session.setState(Session::LOGOUT);
	  }
	}
      }
    }

    // If a syntax error was detected, we skip all characters in the
    // input stream up to and including '\n'.
    if (serverStatus == RequestRejected) {
      bincClient << clientRequest.getTag() << " NO "
		 << session.getResponseCode()
		 << clientRequest.getName() << " failed: " 
		 << session.getLastError() << endl;
    } else if (serverStatus == RequestError) {
      bincClient << "* BAD "
		 << session.getLastError() << endl;
      skipToNextRequest = true;
    } else if (serverStatus == OK && session.getState() != Session::LOGOUT) {
      bincClient << clientRequest.getTag() << " OK";
      if (clientRequest.getUidMode()) bincClient << " UID";
      bincClient << " " << session.getResponseCode() 
		 << clientRequest.getName() << " completed";
      if (clientRequest.getContextInfo() != "")
	bincClient << " (" << clientRequest.getContextInfo() << ")";
      bincClient << endl;
    } else {
      // Timeout, ClientDisconnected
      session.setState(Session::LOGOUT);
    }

    bincClient.flush();

    if (skipToNextRequest) {
      if (!bincClient.skipTo('\n')) {
	if (bincClient.getLastError() == IODevice::Timeout)
	  serverStatus = Timeout;
	else
	  serverStatus = ClientDisconnected;
	break;
      }
    }
  } while (session.getState() != Session::LOGOUT);

  string userID = stubMode ? session.getIP() : session.getEnv("USER");

  if (serverStatus == Timeout) {
    bincClient << "* BYE Timeout after " << session.timeout()
	       << " seconds of inactivity." << endl;
    bincClient.flush();
    bincInfo << "<" << userID << "> timed out after "
	     << IDLE_TIMEOUT << "s";
  } else if (serverStatus == ClientDisconnected) {
    bincInfo << "<" << userID << "> disconnected";
  } else {
    if (stubMode) {
      bincInfo << "<" << userID << "> logged out";
    } else {
      bincInfo << "<" << userID << "> disconnected";
    }
  }

  if (stubMode) {
    bincInfo << " (";
    bincInfo << session.getReadBytes() << " read, "
	     << session.getWriteBytes() << " written"
	     << ")" << endl;
  } else {
    bincInfo << " (";
    bincInfo << session.getBodies() << " bodies, "
	     << session.getStatements() << " statements"
	     << ")" << endl;
  }

  return 0;
}
