/* -*- mode:c++;c-basic-offset:2 -*- */
/*  --------------------------------------------------------------------
 *  Filename:
 *    operator-authenticate.cc
 *  
 *  Description:
 *    Implementation of the AUTHENTICATE command.
 *  --------------------------------------------------------------------
 *  Copyright 2002-2005 Andreas Aardal Hanssen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *  --------------------------------------------------------------------
 */
#include <string>

#include "authenticate.h"
#include "base64.h"
#include "convert.h"
#include "depot.h"
#include "iodevice.h"
#include "iofactory.h"
#include "globals.h"
#include "operators.h"
#include "recursivedescent.h"
#include "session.h"

using namespace ::std;
using namespace Binc;

//----------------------------------------------------------------------
AuthenticateOperator::AuthenticateOperator(void)
{
}

//----------------------------------------------------------------------
AuthenticateOperator::~AuthenticateOperator(void)
{
}

//----------------------------------------------------------------------
const string AuthenticateOperator::getName(void) const
{
  return "AUTHENTICATE";
}

//----------------------------------------------------------------------
int AuthenticateOperator::getState(void) const
{
  return Session::NONAUTHENTICATED;
}

//------------------------------------------------------------------------
Operator::ProcessResult AuthenticateOperator::process(Depot &depot, 
						      Request &command)
{
  Session &session = Session::getInstance();

  string authtype = command.getAuthType();
  uppercase(authtype);

  string username;
  string password;

  // for now, we only support LOGIN.
  if (authtype == "LOGIN") {
    // we only allow this type of authentication over a plain
    // connection if it is passed as argument or given in the conf
    // file.
    if (!session.command.ssl 
	&& !session.hasEnv("ALLOW_NONSSL_PLAINTEXT_LOGINS")) {
      session.setLastError("Plain text password authentication"
			   " is disallowed. Please try enabling SSL"
			   " or TLS in your mail client.");
      return NO;
    }
    
    bincClient << "+ " << base64encode("User Name") << endl;
    bincClient.flush();

    // Read user name
    for (;;) {
      char c;
      if (!bincClient.readChar(&c))
	return BAD;

      if (c == '\n')
	break;

      username += c;
    }
    
    if (username != "" && username[0] == '*') {
      session.setLastError("Authentication cancelled by user");
      return NO;
    }
    
    bincClient << "+ " << base64encode("Password") << endl;
    bincClient.flush();

    // Read password    
    for (;;) {
      char c;
      if (!bincClient.readChar(&c));
	return BAD;

      if (c == '\n')
	break;
       
      password += c;
    }
    
    if (password != "" && password[0] == '*') {
      session.setLastError("Authentication cancelled by user");
      return NO;
    }

    username = base64decode(username);
    password = base64decode(password);

  } else if (authtype == "PLAIN") {
    // we only allow this type of authentication over an SSL encrypted
    // connection.
    if (!session.command.ssl 
	&& !session.hasEnv("ALLOW_NONSSL_PLAINTEXT_LOGINS")) {
      session.setLastError("Plain text password authentication"
			   " is disallowed. Please try enabling SSL"
			   " or TLS in your mail client.");
      return NO;
    }

    bincClient << "+ " << endl;
    bincClient.flush();
    
    string b64;
    for (;;) {
      char c;
      if (!bincClient.readChar(&c)) {
	session.setLastError("unexpected EOF");
	return BAD;
      }

      if (c == '\n')
	break;
      
      b64 += c;
    }

    if (b64.size() >= 1 && b64[0] == '*') {
      session.setLastError("Authentication cancelled by user");
      return NO;
    }

    string plain = base64decode(b64);
    string::size_type pos;
    if ((pos = plain.find('\0')) == string::npos) {
      session.setLastError("Authentication failed. In PLAIN mode,"
			   " there must be at least two null characters"
			   " in the input string, but none were found");
      return NO;
    }

    plain = plain.substr(pos + 1);
    if ((pos = plain.find('\0')) == string::npos) {
      session.setLastError("Authentication failed. In PLAIN mode,"
			   " there must be at least two null characters"
			   " in the input string, but only one was found");
      return NO;
    }

    username = plain.substr(0, pos);
    password = plain.substr(pos + 1);
  } else {
    session.setLastError("The authentication method " 
			 + toImapString(authtype) + " is not supported."
			 " Please try again with a different method."
			 " There is built in support for \"PLAIN\""
			 " and \"LOGIN\".");
    return NO;
  }

  putenv(strdup(("BINCIMAP_LOGIN=AUTHENTICATE+" + command.getTag()).c_str()));

  // the authenticate function calls a stub which does the actual
  // authentication. the function returns 0 (success), 1 (internal
  // error) or 2 (failed)
  switch (authenticate(depot,
		       (const string &)username,
		       (const string &)password)) {
  case 1:
    session.setLastError("An internal error occurred when you attempted"
			 " to log in to the IMAP server. Please contact"
			 " your system administrator.");
    return NO;
  case 2:
    session.setLastError("Login failed. Either your user name"
			 " or your password was wrong. Please try again,"
			 " and if the problem persists, please contact"
			 " your system administrator.");
    return NO;
  case 3:    
    bincClient << "* BYE Timeout after " << IDLE_TIMEOUT
	       << " seconds of inactivity." << endl;
    break;
  case -1:
    bincClient << "* BYE The server died unexpectedly. Please contact "
      "your system administrator for more information." << endl;
    break;

  }

  // auth was ok. go to logout state
  session.setState(Session::LOGOUT);
  return NOTHING;
}


//----------------------------------------------------------------------
Operator::ParseResult AuthenticateOperator::parse(Request &c_in) const
{
  Session &session = Session::getInstance();

  if (c_in.getUidMode())
    return REJECT;

  Operator::ParseResult res;

  if ((res = expectSPACE()) != ACCEPT) {
    session.setLastError("Expected single SPACE after AUTHENTICATE");
    return res;
  }

  string authtype;
  if ((res = expectAtom(authtype)) != ACCEPT) {
    session.setLastError("Expected auth_type after AUTHENTICATE SPACE");
    return ERROR;
  }

  if ((res = expectCRLF()) != ACCEPT) {
    session.setLastError("Expected CRLF after AUTHENTICATE SPACE auth_type");
    return res;
  }

  c_in.setAuthType(authtype);

  c_in.setName("AUTHENTICATE");
  return ACCEPT;
}
