/*-*-mode:c++-*-*/
/*  --------------------------------------------------------------------
 *  Filename:
 *    src/stdiodevice.h
 *  
 *  Description:
 *    Declaration of the StdIODevice class.
 *  --------------------------------------------------------------------
 *  Copyright 2002, 2003 Andreas Aardal Hanssen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *  --------------------------------------------------------------------
 */
#ifndef stdiodevice_h_included
#define stdiodevice_h_included

#include "iodevice.h"

namespace Binc {
  class StdIODevice : public IODevice {
  public:
    StdIODevice(int flags);
    ~StdIODevice();

    std::string service(void) const;

    bool canRead(void) const;

  protected:
    bool waitForWrite(void) const;
    bool waitForRead(void) const;

    WriteResult write(void);
    bool fillInputBuffer(void);   
  };
}

#endif
