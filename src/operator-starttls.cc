/* -*- mode:c++;c-basic-offset:2 -*- */
/*  --------------------------------------------------------------------
 *  Filename:
 *    operator-starttls.cc
 *  
 *  Description:
 *    Implementation of the STARTTLS command.
 *  --------------------------------------------------------------------
 *  Copyright 2002-2005 Andreas Aardal Hanssen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *  --------------------------------------------------------------------
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef WITH_SSL

#include <string>
#include <iostream>

#include "recursivedescent.h"
#include "iodevice.h"
#include "iofactory.h"
#include "session.h"
#include "ssldevice.h"
#include "depot.h"
#include "operators.h"

using namespace ::std;
using namespace Binc;

//----------------------------------------------------------------------
StarttlsOperator::StarttlsOperator(void)
{
}

//----------------------------------------------------------------------
StarttlsOperator::~StarttlsOperator(void)
{
}

//----------------------------------------------------------------------
const string StarttlsOperator::getName(void) const
{
  return "STARTTLS";
}

//----------------------------------------------------------------------
int StarttlsOperator::getState(void) const
{
  return Session::NONAUTHENTICATED 
    | Session::AUTHENTICATED 
    | Session::SELECTED;
}

//------------------------------------------------------------------------
Operator::ProcessResult StarttlsOperator::process(Depot &depot,
						  Request &command)
{
  Session &session = Session::getInstance();
  if (session.command.ssl) {
    session.setLastError("Already in TLS mode");
    return BAD;
  }

  bincClient << command.getTag() 
      << " OK STARTTLS completed, begin TLS negotiation now" << endl;
  bincClient.flush();
   
  SSLDevice *sslDevice;
  sslDevice = dynamic_cast<SSLDevice *>(&IOFactory::getClient());
  if (!sslDevice) {
    // Can this ever happen?
    session.setLastError("An internal error occurred when"
			 " entering SSL mode. ");
    return NO;
  } else {
    if (!sslDevice->initSSL()) {
      session.setLastError(sslDevice->getLastErrorString());
      return NO;
    }
  }

  session.command.ssl = true;

  return NOTHING;
}

//----------------------------------------------------------------------
Operator::ParseResult StarttlsOperator::parse(Request &c_in) const
{
  Session &session = Session::getInstance();

  if (c_in.getUidMode())
    return REJECT;

  Operator::ParseResult res;
  if ((res = expectCRLF()) != ACCEPT) {
    session.setLastError("Expected CRLF");
    return ERROR;
  } else
    return ACCEPT;
}

#endif
