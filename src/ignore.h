/**
 * Simple helper macro for explicitly ignoring return values. You shouldn't
 * use this unless you know what you're doing.
 */
#ifndef IGNORE_H
#define IGNORE_H

template <typename T>
inline void ignore_rval(T const &)
{
}

#endif
