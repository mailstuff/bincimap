/* -*- mode:c++;c-basic-offset:2 -*- */
/*  --------------------------------------------------------------------
 *  Filename:
 *    pendingupdates.cc
 *  
 *  Description:
 *    <--->
 *  --------------------------------------------------------------------
 *  Copyright 2002-2005 Andreas Aardal Hanssen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *  --------------------------------------------------------------------
 */
#include <iostream>
#include <string>
#include <vector>

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "iodevice.h"
#include "iofactory.h"
#include "mailbox.h"
#include "message.h"
#include "pendingupdates.h"
#include "session.h"

using namespace ::std;
using namespace Binc;

//------------------------------------------------------------------------
PendingUpdates::PendingUpdates(void) : expunges(), flagupdates()
{
  recent = 0;
  exists = 0;

  newrecent = false;
  newexists = false;
}

//------------------------------------------------------------------------
PendingUpdates::~PendingUpdates(void)
{
}

//------------------------------------------------------------------------
void PendingUpdates::addExpunged(unsigned int uid)
{
  expunges.push_back(uid);
}

//------------------------------------------------------------------------
void PendingUpdates::addFlagUpdates(unsigned int sqnr, unsigned int uid, unsigned int flags, const vector<string> &cflags)
{
  flagupdates[sqnr] = flags;
  sqnrtouid[sqnr] = uid;
  sqnrtocflags[sqnr] = cflags;
}

//------------------------------------------------------------------------
void PendingUpdates::setExists(unsigned int n)
{
  exists = n;
  newexists = true;
}

//------------------------------------------------------------------------
void PendingUpdates::setRecent(unsigned int n)
{
  recent = n;
  newrecent = true;
}

//------------------------------------------------------------------------
unsigned int PendingUpdates::getExists(void) const
{
  return exists;
}

//------------------------------------------------------------------------
unsigned int PendingUpdates::getRecent(void) const
{
  return recent;
}

//------------------------------------------------------------------------
bool PendingUpdates::newExists(void) const
{
  return newexists;
}

//------------------------------------------------------------------------
bool PendingUpdates::newRecent(void) const
{
  return newrecent;
}

//------------------------------------------------------------------------
PendingUpdates::expunged_const_iterator::expunged_const_iterator(void)
{
}

//------------------------------------------------------------------------
PendingUpdates::expunged_const_iterator::expunged_const_iterator(vector<unsigned int>::iterator i) : internal(i)
{
}

//------------------------------------------------------------------------
unsigned int PendingUpdates::expunged_const_iterator::operator * (void) const
{
  return *internal;
}

//------------------------------------------------------------------------
void PendingUpdates::expunged_const_iterator::operator ++ (void)
{
  ++internal;
}

//------------------------------------------------------------------------
bool PendingUpdates::expunged_const_iterator::operator == (expunged_const_iterator i) const
{
  return internal == i.internal;
}

//------------------------------------------------------------------------
bool PendingUpdates::expunged_const_iterator::operator != (expunged_const_iterator i) const
{
  return internal != i.internal;
}

//------------------------------------------------------------------------
PendingUpdates::expunged_const_iterator PendingUpdates::beginExpunged(void)
{
  return expunged_const_iterator(expunges.begin());
}

//------------------------------------------------------------------------
PendingUpdates::expunged_const_iterator PendingUpdates::endExpunged(void)
{
  return expunged_const_iterator(expunges.end());
}

//------------------------------------------------------------------------
PendingUpdates::flagupdates_const_iterator::flagupdates_const_iterator(void)
{
}

//------------------------------------------------------------------------
PendingUpdates::flagupdates_const_iterator::flagupdates_const_iterator(map<unsigned int, unsigned int>::iterator i, map<unsigned int, vector<string> > *j, map<unsigned int, unsigned int> *sqnrmap) : internal(i), sqnrtocflags(j)
{
  sqnrtouid = sqnrmap;
}

//------------------------------------------------------------------------
void PendingUpdates::flagupdates_const_iterator::operator ++ (void)
{
  ++internal;
}

//------------------------------------------------------------------------
bool PendingUpdates::flagupdates_const_iterator::operator != (flagupdates_const_iterator i) const
{
  return internal != i.internal;
}

//------------------------------------------------------------------------
PendingUpdates::flagupdates_const_iterator PendingUpdates::beginFlagUpdates(void)
{
  return flagupdates_const_iterator(flagupdates.begin(), &sqnrtocflags, &sqnrtouid);
}

//------------------------------------------------------------------------
PendingUpdates::flagupdates_const_iterator PendingUpdates::endFlagUpdates(void)
{
  return flagupdates_const_iterator(flagupdates.end(), &sqnrtocflags, &sqnrtouid);
}

//------------------------------------------------------------------------
unsigned int PendingUpdates::flagupdates_const_iterator::first(void) const
{
  return internal->first;
}

//------------------------------------------------------------------------
unsigned int PendingUpdates::flagupdates_const_iterator::second(void) const
{
  return internal->second;
}

//------------------------------------------------------------------------
vector<string> PendingUpdates::flagupdates_const_iterator::getCustomFlags(void) const
{
  return (*sqnrtocflags)[internal->first];
}

//------------------------------------------------------------------------
unsigned int PendingUpdates::flagupdates_const_iterator::getUID(void) const
{
  return (*sqnrtouid)[internal->first];
}

//--------------------------------------------------------------------
bool Binc::pendingUpdates(Mailbox *mailbox, int type, bool rescan, bool showAll,
			  bool forceScan, bool uidfetchflags)
{
  Session &session = Session::getInstance();

  if (mailbox == 0)
    return true;

  PendingUpdates p;
  if (!mailbox->getUpdates(rescan, type, p, forceScan)) {
    session.setLastError(mailbox->getLastError());
    return false;
  }

  if (type & PendingUpdates::EXPUNGE) {
    PendingUpdates::expunged_const_iterator i = p.beginExpunged();
    PendingUpdates::expunged_const_iterator e = p.endExpunged();

    while (i != e) {
      bincClient << "* " << *i << " EXPUNGE" << endl;
      ++i;
    }
  }

  if (((type & PendingUpdates::EXISTS) && p.newExists()) || showAll)
    bincClient << "* " << p.getExists() << " EXISTS" << endl;

  if (((type & PendingUpdates::RECENT) && p.newRecent()) || showAll)
    bincClient << "* " << p.getRecent() << " RECENT" << endl;

  if (type & PendingUpdates::FLAGS) {
    PendingUpdates::flagupdates_const_iterator i = p.beginFlagUpdates();
    PendingUpdates::flagupdates_const_iterator e = p.endFlagUpdates();

    while (i != e) {
      int flags = i.second();

      vector<string> flagv;
      if (flags & Message::F_SEEN) flagv.push_back("\\Seen");
      if (flags & Message::F_ANSWERED) flagv.push_back("\\Answered");
      if (flags & Message::F_DELETED) flagv.push_back("\\Deleted");
      if (flags & Message::F_DRAFT) flagv.push_back("\\Draft");
      if (flags & Message::F_RECENT) flagv.push_back("\\Recent");
      if (flags & Message::F_FLAGGED) flagv.push_back("\\Flagged");

      bincClient << "* " << i.first() << " FETCH (FLAGS (";
      for (vector<string>::const_iterator k
	     = flagv.begin(); k != flagv.end(); ++k) {
	if (k != flagv.begin())
	  bincClient << " ";
	bincClient << *k;
      }

      vector<string> customFlags = i.getCustomFlags();
      for (vector<string>::const_iterator it = customFlags.begin();
	   it != customFlags.end(); ++it) {
	if (flagv.size() > 0 || it != customFlags.begin())
	  bincClient << " ";
	bincClient << *it;
      }
      
      bincClient << ")";
      if (uidfetchflags)
	bincClient << " UID " << i.getUID();

      bincClient << ")" << endl;

      ++i;
    }
  }

  return true;
}
