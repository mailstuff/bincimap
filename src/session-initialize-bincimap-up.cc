/* -*- mode:c++;c-basic-offset:2 -*- */
/*  --------------------------------------------------------------------
 *  Filename:
 *    session-initialize-bincimap-up.cc
 *  
 *  Description:
 *    <--->
 *  --------------------------------------------------------------------
 *  Copyright 2002-2005 Andreas Aardal Hanssen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *  --------------------------------------------------------------------
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <syslog.h>
#include <ctype.h>

#include "broker.h"
#include "convert.h"
#include "globals.h"
#include "iodevice.h"
#include "iofactory.h"
#include "multilogdevice.h"
#include "session.h"
#include "ssldevice.h"
#include "stdiodevice.h"
#include "syslogdevice.h"
#include "tools.h"

#include <string>
#include <map>

using namespace ::std;
using namespace Binc;

extern char **environ;

//----------------------------------------------------------------------
bool Session::initialize(int argc, char *argv[])
{
  IOFactory &ioFactory = IOFactory::getInstance();

  IODevice *stdioDevice = new StdIODevice(IODevice::IsEnabled);
  stdioDevice->setFlags(IODevice::HasOutputLimit);
  stdioDevice->setMaxOutputBufferSize(TRANSFER_BUFFER_SIZE);
  ioFactory.addDevice(stdioDevice);

  Session &session = Session::getInstance();

  IOFactory::getLogger().clearFlags(IODevice::FlushesOnEndl);

  // Read command line arguments
  if (!session.parseCommandLine(argc, argv))
    return false;

  // Show help if asked for it
  if (session.command.help) {
    printf("%s\n", session.args.usageString().c_str());
    return false;
  }

  // Show help if asked for it
  if (session.command.version) {
    printf("Binc IMAP v" VERSION "\n");
    return false;
  }

  // Let the command line args override the global settings.
  session.assignCommandLineArgs();

  // log settings
  string ipenv = session.getEnv("IP_VARIABLE");
  // Initialize logger
  string ip = getenv(ipenv.c_str()) ? getenv(ipenv.c_str()) :
    getenv("TCPREMOTEIP") ? getenv("TCPREMOTEIP") :
    getenv("REMOTE_HOST") ? getenv("REMOTE_HOST") :
    getenv("REMOTEIP") ? getenv("REMOTEIP") :
    getenv("SSLREMOTEIP") ? getenv("SSLREMOTEIP") : "?";
  session.setIP(ip);

  string logtype = session.getEnv("LOG_TYPE");
  lowercase(logtype);
  trim(logtype);
  if (logtype == "multilog" || logtype == "stderr") {
      MultilogDevice *device = new MultilogDevice(IODevice::IsEnabled
						  | IODevice::FlushesOnEndl);
    ioFactory.addDevice(device);
  } else if (logtype == "" || logtype == "syslog") {
    const string f = session.getEnv("SYSLOG_FACILITY");
    int facility;

    if (isdigit(f[0])) {
      facility = atoi(f);
    } else {
      if (f == "LOG_USER") facility = LOG_USER;
      else if (f == "LOG_LOCAL0") facility = LOG_LOCAL0;
      else if (f == "LOG_LOCAL1") facility = LOG_LOCAL1;
      else if (f == "LOG_LOCAL2") facility = LOG_LOCAL2;
      else if (f == "LOG_LOCAL3") facility = LOG_LOCAL3;
      else if (f == "LOG_LOCAL4") facility = LOG_LOCAL4;
      else if (f == "LOG_LOCAL5") facility = LOG_LOCAL5;
      else if (f == "LOG_LOCAL6") facility = LOG_LOCAL6;
      else if (f == "LOG_LOCAL7") facility = LOG_LOCAL7;
      else facility = LOG_DAEMON;
    }

    SyslogDevice *device = new SyslogDevice(IODevice::IsEnabled
					    | IODevice::FlushesOnEndl,
					    "bincimap-up",
					    LOG_NDELAY | LOG_PID,
					    facility);
    ioFactory.addDevice(device);
  }

  // Now that we know the log type, we can flush.
  IOFactory::getLogger().setFlags(IODevice::FlushesOnEndl);
  IOFactory::getLogger().setOutputLevelLimit(IODevice::InfoLevel);

#ifndef WITH_SSL
  if (hasEnv("START_IN_SSL_MODE")) {
    bincError << "configured to run in SSL mode, but compiled without SSL" << endl;
    return false;
  }
#endif

  BrokerFactory &brokerfactory = BrokerFactory::getInstance();

  brokerfactory.assign("AUTHENTICATE", new AuthenticateOperator());
  brokerfactory.assign("CAPABILITY", new CapabilityOperator());
  brokerfactory.assign("LOGIN", new LoginOperator());
  brokerfactory.assign("LOGOUT", new LogoutOperator());
  brokerfactory.assign("NOOP", new NoopOperator());
#ifdef WITH_SSL
  brokerfactory.assign("STARTTLS", new StarttlsOperator());
#endif

#ifdef WITH_SSL
  // Set SSL mode if --ssl is passed
  if (session.command.ssl) {
    SSLDevice *sslDevice = new SSLDevice(IODevice::IsEnabled);
    sslDevice->setPathToPrivateKey(session.getEnv("SSL_PRIVATE_KEY_FILE"));
    sslDevice->setPathToCertificate(session.getEnv("SSL_CERTIFICATE_FILE"));
    sslDevice->setPathToCAFile(session.getEnv("SSL_CA_FILE"));
    sslDevice->setPathToCADir(session.getEnv("SSL_CA_PATH"));
    sslDevice->setClientCertificateRequired(session.getEnv("SSL_VERIFY_PEER") != "");

    if (!sslDevice->initSSL()) {
      bincInfo << sslDevice->getLastErrorString() << endl;
      return false;
    }

    ioFactory.addDevice(sslDevice);
  }
#endif

  bincClient.setTimeout(60);

  session.setState(Session::NONAUTHENTICATED);
  
  // If the depot was not initialized properly, return false.
  return true;
}
