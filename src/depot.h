/* -*- mode:c++;c-basic-offset:2 -*- */
/*  --------------------------------------------------------------------
 *  Filename:
 *    src/depot.h
 *  
 *  Description:
 *    Declaration of the Depot class.
 *  --------------------------------------------------------------------
 *  Copyright 2002-2005 Andreas Aardal Hanssen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *  --------------------------------------------------------------------
 */
#ifndef depot_h_included
#define depot_h_included
#include <map>
#include <string>
#include <vector>

#include <dirent.h>

namespace Binc {

  class Mailbox;
  class Depot;
  class Status;

  //------------------------------------------------------------------
  class DepotFactory {
  private:
    std::vector<Depot *> depots;
    DepotFactory(void);

  public:
    void assign(Depot *);
    Depot *get(const std::string &name) const;

    static DepotFactory &getInstance(void);
    ~DepotFactory(void);
  };

  //------------------------------------------------------------------
  class Depot {
  public:
    //--
    class iterator {
    public:      
      std::string operator * (void) const;
      void operator ++ (void);
      bool operator != (iterator) const;
      bool operator == (iterator) const;

      iterator(void);
      iterator(const iterator &copy);
      iterator(DIR *, struct dirent *);
      ~iterator(void);

      void deref(void);

      iterator &operator =(const iterator &copy);

      friend class Depot;

    private:
      DIR *dirp;
      struct dirent *direntp;
      int *ref;
    };

  private:
    iterator enditerator;
    std::vector<Mailbox *> backends;
    Mailbox *defaultmailbox;
    Mailbox *selectedmailbox;
    std::vector<std::string> subscribed;
    std::string personalNamespace;
    std::string othersNamespace;
    std::string sharedNamespace;

  protected:
    mutable std::string lastError;
    std::string name;
    char delimiter;
    mutable std::map<std::string, Status> mailboxstatuses;

  public:
    virtual iterator begin(const std::string &) const;
    virtual const iterator &end(void) const;

    void setDelimiter(char);
    const char getDelimiter(void) const;

    virtual void assign(Mailbox *);

    bool setDefaultType(const std::string &n);
    Mailbox *getDefault(void) const;
    virtual Mailbox *get(const std::string &path) const;

    virtual bool setSelected(Mailbox *);
    virtual Mailbox *getSelected(void) const;
    void resetSelected(void);

    bool getStatus(const std::string &s_in, Status &dest) const;

    const std::string &getName(void) const;

    virtual const std::string &getPersonalNamespace(void) const;
    virtual const std::string &getOthersNamespace(void) const;
    virtual const std::string &getSharedNamespace(void) const;

    virtual std::string mailboxToFilename(const std::string &m) const = 0;
    virtual std::string filenameToMailbox(const std::string &m) const = 0;

    virtual bool createMailbox(const std::string &m) const;
    virtual bool deleteMailbox(const std::string &m) const;
    virtual bool renameMailbox(const std::string &m, const std::string &n) const;

    const std::string &getLastError(void) const;
    void setLastError(const std::string &error) const;

    virtual std::vector<std::string> getSubscriptions(void) const;
    virtual void subscribeTo(const std::string mailbox);
    virtual bool unsubscribeTo(const std::string mailbox);
    virtual void loadSubscribes(void);
    virtual bool saveSubscribes(void) const;

    //--
    Depot(void);
    Depot(const std::string &name);
    virtual ~Depot(void);
  };

  //------------------------------------------------------------------
  class MaildirPPDepot : public Depot {
  public:
    std::string mailboxToFilename(const std::string &m) const;
    std::string filenameToMailbox(const std::string &m) const;

    const std::string &getPersonalNamespace(void) const;

    //--
    MaildirPPDepot();
    ~MaildirPPDepot();
  private:
    std::string privateNamespace;
  };

  //------------------------------------------------------------------
  class IMAPdirDepot : public Depot {
  public:
    std::string mailboxToFilename(const std::string &m) const;
    std::string filenameToMailbox(const std::string &m) const;

    //--
    IMAPdirDepot();
    ~IMAPdirDepot();
  };

}

#endif
