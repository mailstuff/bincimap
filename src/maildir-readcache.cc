/* -*- mode:c++;c-basic-offset:2 -*- */
/*  --------------------------------------------------------------------
 *  Filename:
 *    maildir.cc
 *  
 *  Description:
 *    Implementation of the Maildir class.
 *  --------------------------------------------------------------------
 *  Copyright 2002-2005 Andreas Aardal Hanssen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *  --------------------------------------------------------------------
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <algorithm>

#include "maildir.h"

#include "convert.h"

using namespace ::std;
using namespace Binc;

//------------------------------------------------------------------------
Maildir::ReadCacheResult Maildir::readCache(void)
{
  index.clearUids();

  const string cachefilename = path + "/bincimap-cache";
  FILE *fp = fopen(cachefilename.c_str(), "r");
  if (!fp) {
    uidvalidity = time(0);
    uidnext = 1;
    messages.clear();
    index.clear();
    newMessages.clear();
    return NoCache;
  }

  char inputBuffer[512];
  if (!fgets(inputBuffer, sizeof(inputBuffer), fp)) {
    fclose(fp);
    uidvalidity = time(0);
    uidnext = 1;
    messages.clear();
    index.clear();
    newMessages.clear();
    return NoCache;
  }

  // terminate the buffer
  inputBuffer[sizeof(inputBuffer) - 1] = '\0';

  char cacheFileVersionBuffer[512];
  unsigned int readUidValidity;
  unsigned int readUidNext;

  if (sscanf(inputBuffer, "%s %u %u", cacheFileVersionBuffer, &readUidValidity, &readUidNext) != 3
      || strcmp(cacheFileVersionBuffer, "BINC-CACHE-1.0") != 0) {
    // bump cache
    fclose(fp);
    uidvalidity = time(0);
    uidnext = 1;
    messages.clear();
    index.clear();
    newMessages.clear();
    return NoCache;
  }

  uidnext = readUidNext;
  uidvalidity = readUidValidity;

  unsigned int readUID;
  unsigned int readSize;
  unsigned int readInternalDate;
  char readUnique[512];
  while (fgets(inputBuffer, sizeof(inputBuffer), fp)) {
    inputBuffer[sizeof(inputBuffer) - 1] = '\0';
    if (sscanf(inputBuffer, "%u %u %u %s", &readUID,
	       &readInternalDate, &readSize, readUnique) != 4) {
      // error in input
      fclose(fp);
      uidvalidity = time(0);
      uidnext = 1;
      messages.clear();
      index.clear();
      newMessages.clear();
      return NoCache;
    }

    vector<string> customFlags;

    char *flagStart = inputBuffer;
    for (int i = 0; flagStart != 0 && *flagStart != '\0' && i < 4; ++i) {
      flagStart = strchr(flagStart, ' ');

      // skip consecutive white space
      while (flagStart && *flagStart == ' ')
	++flagStart;
    }

    // get flags
    while (flagStart) {
      char *lastOffset = flagStart;
      flagStart = strchr(flagStart, ' ');
      if (flagStart) {
	*flagStart = '\0';
	++flagStart;
      }

      customFlags.push_back(lastOffset);

      // skip consecutive white space
      while (flagStart && *flagStart != '\0' && *flagStart == ' ')
	++flagStart;
    }

    MaildirMessage m(*this);
    m.setUnique(readUnique);
    m.setInternalDate(readInternalDate);

    if (index.find(readUnique) == 0) {
      for (vector<string>::const_iterator it = customFlags.begin();
	   it != customFlags.end(); ++it) {
	string tmpFlag = *it;
	trim(tmpFlag, " \n");
	m.setCustomFlag(tmpFlag);
      }

      m.setUID(readUID);
      m.setInternalFlag(MaildirMessage::JustArrived);
      m.setSize(readSize);
      add(m);
    } else {
      // Remember to insert the uid of the message again - we reset this
      // at the top of this function.
      index.insert(readUnique, readUID);
      MaildirMessage &existingMessage = messages.find(readUID)->second;
      
      vector<string> oldCustomFlags = existingMessage.getCustomFlags();
      sort(oldCustomFlags.begin(), oldCustomFlags.end());
      sort(customFlags.begin(), customFlags.end());
      if (oldCustomFlags != customFlags) {
	existingMessage.resetCustomFlags();
	for (vector<string>::const_iterator it = customFlags.begin();
	     it != customFlags.end(); ++it) {
	  string tmpFlag = *it;
	  trim(tmpFlag, " \n");
	  existingMessage.setCustomFlag(tmpFlag);
	}
      }
    }
  }

  fclose(fp);

  return Ok;
}

