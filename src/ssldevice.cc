/*-*-mode:c++-*-*/
/*  --------------------------------------------------------------------
 *  Filename:
 *    src/ssldevice.cc
 *  
 *  Description:
 *    Implementation of the SSLDevice class.
 *  --------------------------------------------------------------------
 *  Copyright 2002, 2003 Andreas Aardal Hanssen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *  --------------------------------------------------------------------
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#ifdef WITH_SSL

#include "ssldevice.h"
#include <string>

#include <sys/types.h>
#include <sys/select.h>
#include <sys/time.h>
#include <unistd.h>
#include <errno.h>

#include <openssl/ssl.h>
#include <openssl/err.h>

using namespace ::std;
using namespace ::Binc;

//------------------------------------------------------------------------
SSLDevice::SSLDevice(int f) : IODevice(f)
{
  clientCertificateRequired = false;
}

//------------------------------------------------------------------------
SSLDevice::~SSLDevice(void)
{
}

//------------------------------------------------------------------------
string SSLDevice::service(void) const
{
  return "client";
}

//------------------------------------------------------------------------
bool SSLDevice::waitForWrite(void) const
{
  fd_set writeMask;
  FD_ZERO(&writeMask);
  FD_SET(fileno(stdout), &writeMask);

  struct timeval tv;
  tv.tv_sec = timeout;
  tv.tv_usec = 0;

  int result = select(fileno(stdout) + 1, 0, &writeMask,
		      0, timeout ? &tv : 0);
  if (result == 0)
      error = Timeout;
  return result > 0;
}

//------------------------------------------------------------------------
bool SSLDevice::waitForRead(void) const
{
  fd_set readMask;
  FD_ZERO(&readMask);
  FD_SET(fileno(stdin), &readMask);

  struct timeval tv;
  tv.tv_sec = timeout;
  tv.tv_usec = 0;

  int result = select(fileno(stdin) + 1, &readMask, 0,
		      0, timeout ? &tv : 0);
  if (result == 0)
      error = Timeout;
  return result > 0;
}

//------------------------------------------------------------------------
IODevice::WriteResult SSLDevice::write(void)
{
  for (;;) {
    int res = ::SSL_write(ssl, outputBuffer.str().c_str(),
			  outputBuffer.getSize());

    if (res > 0)
      if (res < (int) outputBuffer.getSize()) {
	outputBuffer.popString(res);
	return WriteWait;
      } else {
	outputBuffer.clear();
	return WriteDone;
      }

    if (res == 0)
      return WriteError;

    int err = SSL_get_error(ssl, res);
    if (err != SSL_ERROR_WANT_READ && err != SSL_ERROR_WANT_WRITE)
      return WriteError;
  }
}

//------------------------------------------------------------------------
bool SSLDevice::fillInputBuffer(void)
{
  if (!SSL_pending(ssl) && !waitForRead())
    return false;

  char buf[4096];
  for (;;) {
    unsigned int res = SSL_read(ssl, buf, sizeof(buf) - 1);
    if (res > 0) {
      buf[res] = '\0';
      inputBuffer << buf;
      return true;
    }
    
    if (res == 0)
      return false;
    
    int err = SSL_get_error(ssl, res);
    if (err != SSL_ERROR_WANT_READ && err != SSL_ERROR_WANT_WRITE)
      return false;
    
    if (!waitForRead())
      return false;
  }
}

//------------------------------------------------------------------------
void SSLDevice::setPathToPrivateKey(const string &path)
{
  pkey = path;
}

//------------------------------------------------------------------------
void SSLDevice::setPathToCertificate(const string &path)
{
  cert = path;
}

//------------------------------------------------------------------------
void SSLDevice::setPathToCAFile(const string &path)
{
  cafile = path;
}

//------------------------------------------------------------------------
void SSLDevice::setPathToCADir(const string &path)
{
  cadir = path;
}

//------------------------------------------------------------------------
void SSLDevice::setClientCertificateRequired(bool required)
{
  clientCertificateRequired = required;
}

//------------------------------------------------------------------------
bool SSLDevice::initSSL(void)
{
  SSL_library_init();
  SSL_load_error_strings();
  
  OpenSSL_add_ssl_algorithms();
  
  if ((ctx = SSL_CTX_new(SSLv23_server_method())) == 0) {
    errorString = "SSL error: internal error when creating CTX: " 
      + string(ERR_error_string(ERR_get_error(), 0));
    return false;
  }
  
  SSL_CTX_set_options(ctx, SSL_OP_ALL);
  SSL_CTX_set_default_verify_paths(ctx);

  if (!SSL_CTX_use_certificate_file(ctx, cert.c_str(), SSL_FILETYPE_PEM)) {
    errorString = "SSL error: unable to use certificate "
      + cert + ": "
      + string(ERR_error_string(ERR_get_error(), 0));
    return false;
  }
  
  if (!SSL_CTX_use_PrivateKey_file(ctx, pkey.c_str(), SSL_FILETYPE_PEM)) {
    errorString = "SSL error: unable to use private key in PEM file: "
      + pkey + ": "
      + string(ERR_error_string(ERR_get_error(), 0));
    return false;
  }

  if (!SSL_CTX_check_private_key(ctx)) {
    errorString = "SSL error: certificate and private key don't match: "
      + string(ERR_error_string(ERR_get_error(), 0));
    return false;
  }

  if (clientCertificateRequired) {
    SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER 
                       | SSL_VERIFY_FAIL_IF_NO_PEER_CERT, 0);
  } else {
    SSL_CTX_set_verify(ctx, SSL_VERIFY_NONE, 0);
  }
  
  if (cafile != "" || cadir != "") {
    if (!SSL_CTX_load_verify_locations(ctx, cafile == "" ? 0 : cafile.c_str(),
				       cadir == "" ? 0 : cadir.c_str())) {
      errorString = "SSL error: error loading CA data from file \""
	+ cafile + "\"" + " and directory \"" + cadir + "\": "
	+ string(ERR_error_string(ERR_get_error(), 0));
      return false;
    }
  }

  if ((ssl = SSL_new(ctx)) == 0) {
    errorString = "SSL error: when creating SSL object: "
      + string(ERR_error_string(ERR_get_error(), 0));
    return false;
  }

  SSL_clear(ssl);

  SSL_set_rfd(ssl, 0);
  SSL_set_wfd(ssl, 1);
  SSL_set_accept_state(ssl);
   
  fflush(stdout);

  int result = SSL_accept(ssl);
  if (result <= 0) {
    switch (SSL_get_error(ssl, 0)) {
    case SSL_ERROR_NONE: 
      errorString = "Unknown error"; break;
    case SSL_ERROR_ZERO_RETURN:
      errorString = "Connection closed"; break;
    case SSL_ERROR_WANT_READ:
    case SSL_ERROR_WANT_WRITE:
    case SSL_ERROR_WANT_CONNECT: 
      errorString = "Operation did not complete"; break;
    case SSL_ERROR_WANT_X509_LOOKUP:
      errorString = "X509 lookup requested"; break;
    case SSL_ERROR_SYSCALL:
      errorString = "Unexpected EOF"; break;
    case SSL_ERROR_SSL: 
      errorString = "Internal SSL error: ";
      errorString += string(ERR_error_string(ERR_get_error(), 0)); break;
    }

    return false;
  }

  return true;
}

bool SSLDevice::canRead(void) const
{
  return SSL_pending(ssl) > 0;
}
#endif
