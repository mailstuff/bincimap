/*-*-mode:c++-*-*/
/*  --------------------------------------------------------------------
 *  Filename:
 *    src/multilogdevice.cc
 *  
 *  Description:
 *    Implementation of the MultilogDevice class.
 *  --------------------------------------------------------------------
 *  Copyright 2002, 2003 Andreas Aardal Hanssen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *  --------------------------------------------------------------------
 */
#include "multilogdevice.h"
#include <string>

#include <sys/types.h>
#include <sys/select.h>
#include <sys/time.h>
#include <unistd.h>
#include <errno.h>

using namespace ::std;
using namespace ::Binc;

//------------------------------------------------------------------------
MultilogDevice::MultilogDevice(int f) : IODevice(f)
{
}

//------------------------------------------------------------------------
MultilogDevice::~MultilogDevice(void)
{
}

//------------------------------------------------------------------------
string MultilogDevice::service(void) const
{
  return "log";
}

//------------------------------------------------------------------------
bool MultilogDevice::waitForWrite(void) const
{
  fd_set writeMask;
  FD_ZERO(&writeMask);
  FD_SET(fileno(stderr), &writeMask);

  struct timeval tv;
  tv.tv_sec = getTimeout();
  tv.tv_usec = 0;

  int result = select(fileno(stderr) + 1, 0, &writeMask,
		      0, tv.tv_sec ? &tv : 0);

  return result > 0;
}

//------------------------------------------------------------------------
bool MultilogDevice::waitForRead(void) const
{
  return false;
}

//------------------------------------------------------------------------
IODevice::WriteResult MultilogDevice::write(void)
{
  for (;;) {
    ssize_t wrote = ::write(fileno(stderr), outputBuffer.str().c_str(),
			    outputBuffer.getSize());

    if (wrote == -1) {
      if (errno == EINTR)
	continue;
      else
	return WriteError;
    }

    if ((unsigned int) wrote == outputBuffer.getSize()) {
      outputBuffer.clear();
      return WriteDone;
    }

    outputBuffer.popString(wrote);
    return WriteWait;
  }
}

//------------------------------------------------------------------------
bool MultilogDevice::fillInputBuffer(void)
{
  return false;
}
