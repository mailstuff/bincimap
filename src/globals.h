/* -*- mode:c++;c-basic-offset:2 -*- */
/*  --------------------------------------------------------------------
 *  Filename:
 *    src/globals.h
 *  
 *  Description:
 *    Global constants.
 *  --------------------------------------------------------------------
 *  Copyright 2002-2005 Andreas Aardal Hanssen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *  --------------------------------------------------------------------
 */
#ifndef GLOBAL_H_INCLUDED
#define GLOBAL_H_INCLUDED

namespace Binc {
  static const int IDLE_TIMEOUT = 30*60;
  static const int AUTH_TIMEOUT = 60;
  static const int AUTH_PENALTY = 4;
  static const int TRANSFER_TIMEOUT = 20*60;
  static const int TRANSFER_BUFFER_SIZE = 1024;
  static const int INPUT_BUFFER_LIMIT = 8192;

};
#endif
