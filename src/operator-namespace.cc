/* -*- mode:c++;c-basic-offset:2 -*- */
/*  --------------------------------------------------------------------
 *  Filename:
 *    operator-namespace.cc
 *  
 *  Description:
 *    Operator for the NAMESPACE command.
 *  --------------------------------------------------------------------
 *  Copyright 2002-2005 Andreas Aardal Hanssen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *  --------------------------------------------------------------------
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string>
#include <iostream>

#include "depot.h"
#include "iodevice.h"
#include "iofactory.h"
#include "operators.h"
#include "recursivedescent.h"
#include "session.h"

using namespace ::std;
using namespace Binc;

//----------------------------------------------------------------------
NamespaceOperator::NamespaceOperator(void)
{
}

//----------------------------------------------------------------------
NamespaceOperator::~NamespaceOperator(void)
{
}

//----------------------------------------------------------------------
const string NamespaceOperator::getName(void) const
{
  return "NAMESPACE";
}

//----------------------------------------------------------------------
int NamespaceOperator::getState(void) const
{
  return Session::AUTHENTICATED
    | Session::SELECTED;
}

//----------------------------------------------------------------------
Operator::ProcessResult NamespaceOperator::process(Depot &depot,
					      Request &command)
{
  bincClient << "* NAMESPACE ";

  bincClient << "(("; // personal namespace
  bincClient << toImapString(depot.getPersonalNamespace());
  bincClient << " ";
  char c = depot.getDelimiter();
  bincClient << toImapString(string(&c, 1));
  bincClient << "))";

  bincClient << " NIL"; // others' namespaces
  bincClient << " NIL"; // shared namespaces
  bincClient << endl;

  return OK;
}

//----------------------------------------------------------------------
Operator::ParseResult NamespaceOperator::parse(Request &c_in) const
{
  Session &session = Session::getInstance();

  if (c_in.getUidMode())
    return REJECT;

  Operator::ParseResult res;
  if ((res = expectCRLF()) != ACCEPT) {
    session.setLastError("Expected CRLF after NAMESPACE");
    return res;
  }

  c_in.setName("NAMESPACE");
  return ACCEPT;
}
