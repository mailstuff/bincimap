/* -*- mode:c++;c-basic-offset:2 -*- */
/*  --------------------------------------------------------------------
 *  Filename:
 *    session.cc
 *  
 *  Description:
 *    Implementation of the Session class.
 *  --------------------------------------------------------------------
 *  Copyright 2002-2005 Andreas Aardal Hanssen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *  --------------------------------------------------------------------
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <unistd.h>

#include <syslog.h>

#include "argparser.h"
#include "convert.h"
#include "globals.h"
#include "session.h"
#include "tools.h"
#include <string>
#include <map>

using namespace ::std;
using namespace Binc;

extern char **environ;

//----------------------------------------------------------------------
Session::Session(void)
{
  readbytes = 0;
  writebytes = 0;
  statements = 0;
  bodies = 0;
  mailboxchanges = true;
  logfacility = LOG_DAEMON;
}

//----------------------------------------------------------------------
Session &Session::getInstance(void)
{
  static Session session;
  return session;
}

//----------------------------------------------------------------------
const int Session::getState(void) const
{
  return state;
}

//----------------------------------------------------------------------
void Session::setState(int n)
{
  state = n;
}

//----------------------------------------------------------------------
const string &Session::getUserID(void) const
{
  return userid;
}

//----------------------------------------------------------------------
void Session::setUserID(const string &s)
{
  userid = s;
}

//----------------------------------------------------------------------
const string &Session::getIP(void) const
{
  return ip;
}

//----------------------------------------------------------------------
void Session::setIP(const string &s)
{
  ip = s;
}

//----------------------------------------------------------------------
void Session::setLogFacility(int facility)
{
  logfacility = facility;
}

//----------------------------------------------------------------------
int Session::getLogFacility(void) const
{
  return logfacility;
}

//----------------------------------------------------------------------
void Session::addBody(void)
{
  ++bodies;
}

//----------------------------------------------------------------------
void Session::addStatement(void)
{
  ++statements;
}

//----------------------------------------------------------------------
void Session::addReadBytes(int i)
{
  readbytes += i;
}

//----------------------------------------------------------------------
void Session::addWriteBytes(int i)
{
  writebytes += i;
}

//----------------------------------------------------------------------
int Session::getBodies(void) const
{
  return bodies;
}

//----------------------------------------------------------------------
int Session::getStatements(void) const
{
  return statements;
}

//----------------------------------------------------------------------
int Session::getWriteBytes(void) const
{
  return writebytes;
}

//----------------------------------------------------------------------
int Session::getReadBytes(void) const
{
  return readbytes;
}

//----------------------------------------------------------------------
bool Session::parseCommandLine(int argc, char * argv[])
{
  args.addOptional("h|?|help", "Display this help screen", true);
  args.addOptional("version", "Display the version of Binc IMAP", true);
  args.addOptional("a|allow-plain", "Allow authentication when not in SSL", true);
  args.addOptional("v|show-version", "Enable verbose IMAP greeting", false);
  args.addOptional("l|log-type", "Sets the method used for logging", false);
  args.addOptional("i|ip-variable", "Sets the env variable that contains the remote IP", false);
  args.addOptional("d|depot", "Sets the depot type", false);
  args.addOptional("D|delimiter", "Sets the mailbox delimiter", false);
  args.addOptional("s|enable-ssl", "Toggle enabling of SSL", true);

  args.addOptional("c|ssl-cert-file", "Sets the path to the SSL certificate file", false);
  args.addOptional("k|ssl-key-file", "Sets the path to the SSL key file", false);
  args.addOptional("p|ssl-ca-path", "Sets the path to the CA cert file", false);
  args.addOptional("f|ssl-ca-file", "Sets the path to the CA cert directory", false);
  args.addOptional("C|ssl-ciphers", "Sets the SSL cipher list", false);
  args.addOptional("V|ssl-verify-client", "Toggles peer verificatin", true);

  if (!args.parse(argc, argv)) {
    setLastError("Command line error, " + args.errorString());
    return false;
  }

  command.help = args["help"] == "yes";
  command.version = args["version"] == "yes";
  command.ssl = args["enable-ssl"] == "yes";

  unparsedArgs = argv + args.argc();

  return true;
}

//----------------------------------------------------------------------
void Session::assignCommandLineArgs(void)
{
  if (args.hasArg("allow-plain"))
    setEnv("ALLOW_NONSSL_PLAINTEXT_LOGINS", "yes");

  if (args.hasArg("show-version"))
    setEnv("SHOW_VERSION_IN_GREETING", "yes");

  if (args.hasArg("log-type"))
    setEnv("LOG_TYPE", args["log-type"]);

  if (args.hasArg("ip-variable"))
    setEnv("IP_VARIABLE", args["ip-variable"]);

#if defined CHROOT_JAIL_PATH
    setEnv("CHROOT_JAIL_PATH", CHROOT_JAIL_PATH);
#endif

#if defined CHROOT_JAIL_PATH
    setEnv("CHROOT_JAIL_USER", CHROOT_JAIL_USER);
#endif

#if defined CHROOT_JAIL_PATH
    setEnv("CHROOT_JAIL_GROUP", CHROOT_JAIL_GROUP);
#endif

  if (args.hasArg("depot"))
    setEnv("DEPOT", args["depot"]);

  if (args.hasArg("delimiter"))
    setEnv("DELIMITER", args["delimiter"]);

  if (args.hasArg("enable-ssl"))
    setEnv("START_IN_SSL_MODE", args["enable-ssl"]);

  if (args.hasArg("ssl-cert-file"))
    setEnv("SSL_CERTIFICATE_FILE", args["ssl-cert-file"]);

  if (args.hasArg("ssl-key-file"))
    setEnv("SSL_PRIVATE_KEY_FILE", args["ssl-key-file"]);

  if (args.hasArg("ssl-ca-path"))
    setEnv("SSL_CA_PATH", args["ssl-ca-path"]);

  if (args.hasArg("ssl-ca-file"))
    setEnv("SSL_CA_FILE", args["ssl-ca-file"]);

  if (args.hasArg("ssl-ciphers"))
    setEnv("SSL_CIPHERS", args["ssl-ciphers"]);

  if (args.hasArg("ssl-verify-client"))
    setEnv("SSL_VERIFY_CLIENT", "yes");
}

//----------------------------------------------------------------------
const string &Session::getLastError(void) const
{
  return lastError;
}

//----------------------------------------------------------------------
void Session::setLastError(const string &error) const
{
  lastError = error;
}

//----------------------------------------------------------------------
const string &Session::getResponseCode(void) const
{
  return responseCode;
}

//----------------------------------------------------------------------
void Session::setResponseCode(const string &code) const
{
  responseCode = "[" + code + "] ";
}

//----------------------------------------------------------------------
void Session::clearResponseCode(void) const
{
  responseCode = "";
}

//----------------------------------------------------------------------
pid_t Session::getPid(void)
{
  if (pid == 0)
    pid = getpid();

  return pid;
}

//----------------------------------------------------------------------
const std::string &Session::getHostname(void)
{
  if (hostname == "") {
    char hostnamec[512];
    int hostnamelen = gethostname(hostnamec, sizeof(hostnamec));
    if (hostnamelen == -1 || hostnamelen == sizeof(hostnamec))
      strcpy(hostnamec, "localhost");
    hostnamec[511] = '\0';

    char *c;
    while ((c = strchr(hostnamec, '/')) != 0) *c = '\057';
    while ((c = strchr(hostnamec, ':')) != 0) *c = '\072';
    
    hostname = hostnamec;
  }

  return hostname;
}

//----------------------------------------------------------------------
int Session::timeout() const
{
  return state == NONAUTHENTICATED ? AUTH_TIMEOUT : IDLE_TIMEOUT;
}

//----------------------------------------------------------------------
bool Session::hasEnv(const string &key) const
{
  return getenv(key.c_str()) != 0;
}

//----------------------------------------------------------------------
string Session::getEnv(const string &key)
{
  char *c = getenv(key.c_str());
  return c ? c : "";
}

//----------------------------------------------------------------------
void Session::setEnv(const string &key, const string &value)
{
  string env = key + "=" + value;
  putenv(strdup(env.c_str()));
}
