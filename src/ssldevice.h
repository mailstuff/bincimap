/*-*-mode:c++-*-*/
/*  --------------------------------------------------------------------
 *  Filename:
 *    src/ssldevice.h
 *  
 *  Description:
 *    Declaration of the SSLDevice class.
 *  --------------------------------------------------------------------
 *  Copyright 2002, 2003 Andreas Aardal Hanssen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *  --------------------------------------------------------------------
 */
#ifndef ssldevice_h_included
#define ssldevice_h_included

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#ifdef WITH_SSL

#include "iodevice.h"
#include <openssl/ssl.h>

namespace Binc {
  class SSLDevice : public IODevice {
  public:
    SSLDevice(int flags);
    ~SSLDevice();

    std::string service(void) const;

    void setPathToPrivateKey(const std::string &path);
    void setPathToCertificate(const std::string &path);
    void setPathToCAFile(const std::string &path);
    void setPathToCADir(const std::string &path);
    void setClientCertificateRequired(bool required);

    bool initSSL(void);

    bool canRead(void) const;

  protected:
    bool waitForWrite(void) const;
    bool waitForRead(void) const;

    WriteResult write(void);
    bool fillInputBuffer(void);   

  private:
    SSL *ssl;
    SSL_CTX *ctx;

    std::string pkey;
    std::string cert;
    std::string cafile;
    std::string cadir;
    bool clientCertificateRequired;
  };
}

#endif
#endif
