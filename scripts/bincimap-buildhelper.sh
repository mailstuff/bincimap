#!/bin/bash

# Welcome to the Binc IMAP Build helper program. This script should be
# run several times in a daily cronjob. It doesn't matter what time of
# day it is run. The following line works fine for most systems:
#
# 0 0,4,8,12,16,20 * * * /usr/local/bin/bincimap-buildhelper.sh
# 
# The script downloads today's snapshot package from one of the Binc
# IMAP mirrors. It then unpacks the package and tries to build it. If
# the build fails, an email is sent to <binc-bible@bincimap.org> with
# a report of the failed build.
#
# It is important that the sendmail program is working properly on your
# system. To check that this script works, change the RECIPIENT_EMAIL
# variable below to one of your own email addresses. Run the script, and
# see if you receive an email.
#
# Please edit your personal settings below before running the script.
# Take special care to get the DISTRIBUTION variable right, as it tells
# on what type of system Binc IMAP failed to compile.
#
# If you want a copy of the report this script sends, uncomment the
# CC_TO_CONTRIBUTOR line below.
#
# Happy IMAPing! - Andy :-)

#-------------------------------------------------------------------------
# Personal settings. Uncomment and provide proper values.
#-------------------------------------------------------------------------

#CONTRIBUTOR_NAME="John Smith"
#CONTRIBUTOR_EMAIL="john.smith@example.com"
#DISTRIBUTION="Debian Testing 3.0 ("`uname -a`")"
#RECIPIENT_EMAIL="binc-bible@bincimap.org"
#CC_TO_CONTRIBUTOR=1
SENDMAIL=/usr/sbin/sendmail
GAWK=gawk
GTAR=gtar

#-------------------------------------------------------------------------
# End of personal settings.
#-------------------------------------------------------------------------

VERBOSE=no
if [ "x$1" = "x-v" ]; then
    VERBOSE=yes
fi

if [ "x$CONTRIBUTOR_NAME" = "x" -o "x$CONTRIBUTOR_EMAIL" = "x" \
     -o "x$DISTRIBUTION" = "x" -o "x$RECIPIENT_EMAIL" = "x" \
     -o "x$SENDMAIL" = "x" ]; then
    echo "Please edit the $0 script to set up some"
    echo "basic personal settings before running this"
    echo "script."
    exit 1
fi

# Internal variables.
MAINSITE=http://www.bincimap.org
MIRROR=$MAINSITE
MIRRORFILE=/tmp/bincimap-mirrors.txt
WORKINGDIR=/tmp/bincimap-buildhelper
LOGFILE=/tmp/bincimap-buildhelper.log

# Search for either fetch or wget.
GETTARBALL=""
FIND=`which fetch 2>/dev/null`;
if [ -x "$FIND" ]; then
    GETTARBALL=$FIND
    GETMIRRORS="$GETTARBALL -o $MIRRORFILE $MAINSITE/mirrors.txt"
fi

if [ "x$GETTARBALL" = "x" ]; then
    FIND=`which wget 2>/dev/null`;
    if [ -x "$FIND" ]; then
        GETTARBALL=$FIND
        GETMIRRORS="$GETTARBALL $MAINSITE/mirrors.txt -O$MIRRORFILE"
    fi
fi

if [ "x$GETTARBALL" = "x" ]; then
    echo "This script requires either wget or fetch to be "
    echo "installed and in path. Either install one of the tools, "
    echo "or edit this script to match any other download tool "
    echo "you can use."
    exit 1
fi

if [ $VERBOSE = "yes" ]; then echo "Using download tool: $GETTARBALL"; fi

# Enter the safe spot.
if [ $VERBOSE = "yes" ]; then echo "Change to working dir: $WORKINGDIR"; fi
if [ -d $WORKINGDIR ]; then
    echo "Error: $WORKINGDIR already exists."
    echo "This could be caused by this script getting interrupted"
    echo "before it completed, for example by a crash. Please investigate,"
    echo "then remove $WORKINGDIR before continuing."
    exit 1
fi
rm -rf $WORKINGDIR
mkdir -p $WORKINGDIR
cd $WORKINGDIR

# Choose a random mirror.
if $GETMIRRORS >/dev/null 2>&1; then
    MIRROR=`$GAWK '{system("sleep 1"); srand(); printf "%g %s\n", rand(), $0 }' <$MIRRORFILE|sort -n|tail -1|$GAWK {'print $2'}`
    rm $MIRRORFILE
fi
if [ $VERBOSE = "yes" ]; then echo "Use mirror site: $MIRROR"; fi

# Takes one argument; the name of a snapshot package without the
# .tar.gz extension.
testPackage()
{
    if ! (grep $1 $LOGFILE >/dev/null 2>&1) then
        if [ $VERBOSE = "yes" ]; then echo "Test package: $1"; fi
        # Download the package if it exists.
        rm -f report.txt
        if $GETTARBALL $MIRROR/dl/tarballs/snapshots/$1.tar.gz >>report.txt 2>&1; then
            if [ $VERBOSE = "yes" ]; then echo "> Downloaded package"; fi
            echo $1 >>$LOGFILE
            if [ $VERBOSE = "yes" ]; then echo "> Starting build"; fi
            if ! ($GTAR xzf $1.tar.gz >>report.txt 2>&1 \
                  && cd $1 && ./configure >>../report.txt 2>&1 \
                  && make >> ../report.txt 2>&1); then
                if [ $VERBOSE = "yes" ]; then echo "> Build failed"; fi
                echo "From: $CONTRIBUTOR_NAME <$CONTRIBUTOR_EMAIL>" > report-email.txt
                echo To: Binc IMAP Build Log Emails \<$RECIPIENT_EMAIL\> >> report-email.txt
                echo Subject: Failure, $1 $DISTRIBUTION >> report-email.txt
                echo >> report-email.txt
                echo Build failure: $1 >> report-email.txt
                echo Distribution:  $DISTRIBUTION. >> report-email.txt
                echo Transcript: >> report-email.txt
                echo >> report-email.txt
                cat report.txt >> report-email.txt
            else
                if [ $VERBOSE = "yes" ]; then echo "> Build was successfull"; fi
                echo "From: $CONTRIBUTOR_NAME <$CONTRIBUTOR_EMAIL>" > report-email.txt
                echo To: Binc IMAP Build Log Emails \<$RECIPIENT_EMAIL\> >> report-email.txt
                echo Subject: Success, $1 $DISTRIBUTION >> report-email.txt
                echo >> report-email.txt
                echo Build success: $1 >> report-email.txt
                echo Distribution:  $DISTRIBUTION. >> report-email.txt
            fi
            if [ "x$CC_TO_CONTRIBUTOR" != "x" ]; then
                RECIPIENT_EMAIL="$RECIPIENT_EMAIL $CONTRIBUTOR_EMAIL"
            fi
            $SENDMAIL -f"$CONTRIBUTOR_EMAIL" $RECIPIENT_EMAIL < report-email.txt
        else
            if [ $VERBOSE = "yes" ]; then
                echo "> Package not (yet) available on the mirror";
            fi
        fi
    else
        if [ $VERBOSE = "yes" ]; then echo "> Skip package: already tested"; fi
    fi
}

# Test both the trunk and the 1.2 branch.
testPackage "bincimap-trunk-`date +'%Y-%m-%d'`";
testPackage "bincimap-1.2-`date +'%Y-%m-%d'`";

# Clean up.
cd /
rm -rf $WORKINGDIR
